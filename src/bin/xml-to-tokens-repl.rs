use std::io;
use std::io::Write;
use xml_tokens::xml1_0::tokenizer::Tokenizer;

/// A simple REPL (Read Execute Print Loop) programme to aid in the
/// testing and debugging of the `xml_tokens` library.
///
/// This programme takes XML markup as input and prints the
/// corresponding XML tokens (one per line) as output.
///
/// Entering `"exit"` quits the programme.
fn main() {
    println!("XML to Tokens REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                let tokenizer = Tokenizer::new(input.chars());
                for token in tokenizer {
                    println!("{}", token);
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
