use crate::scanner::Scanner;
use crate::xml1_0::doctype_decl::DoctypeDecl;
use crate::xml1_0::element::ETag;
use crate::xml1_0::element::EmptyOrSTag;
use crate::xml1_0::pi::Pi;
use crate::xml1_0::reference::DecCharRef;
use crate::xml1_0::reference::EntityRef;
use crate::xml1_0::reference::HexCharRef;
use crate::xml1_0::token::Token;
use crate::xml1_0::CdataSection;
use crate::xml1_0::Comment;
use crate::xml1_0::Text;
use crate::xml1_0::XmlDecl;
use crate::Error;
use std::str::Chars;

/// Represents the production rule which the tokenizer should be
/// executing next.
#[derive(Debug, PartialEq)]
enum Rule {
    Document,
    Prolog,
    RootElement,
    MiscStar,
    XmlDeclEroteme,
    DoctypedeclMiscStarEroteme,
    Content,
    Error,
}

/// Converts a string of characters to tokens which can be iterated
/// over.
pub struct Tokenizer<'a> {
    scanner: Scanner<'a>,
    rule_stack: Vec<Rule>,
}
impl<'a> Iterator for Tokenizer<'a> {
    type Item = Token;

    /// Get the next token.
    fn next(&mut self) -> Option<Self::Item> {
        match self.rule_stack.last() {
            Some(rule) => match rule {
                Rule::Document => self.document(),
                Rule::Prolog => self.prolog(),
                Rule::RootElement => Some(self.root_element()),
                Rule::MiscStar => self.misc_star(),
                Rule::XmlDeclEroteme => self.xml_decl_eroteme(),
                Rule::DoctypedeclMiscStarEroteme => self.doctypedecl_misc_star_eroteme(),
                Rule::Content => Some(self.content()),
                Rule::Error => None,
            },
            None => None,
        }
    }
}
impl<'a> Tokenizer<'a> {
    /// Create a new tokenizer, using `chars` as input.
    pub fn new(chars: Chars<'a>) -> Tokenizer<'a> {
        Tokenizer {
            scanner: Scanner::new(chars),
            rule_stack: vec![Rule::Document],
        }
    }

    /// Implementation of the [`document` production
    /// rule](https://www.w3.org/TR/xml/#document).
    fn document(&mut self) -> Option<Token> {
        self.replace_production_rule(
            Rule::Document,
            vec![Rule::Prolog, Rule::RootElement, Rule::MiscStar],
        );

        self.next()
    }

    /// Implementation of the [`prolog` production
    /// rule](https://www.w3.org/TR/xml/#NT-prolog).
    fn prolog(&mut self) -> Option<Token> {
        self.replace_production_rule(
            Rule::Prolog,
            vec![
                Rule::XmlDeclEroteme,
                Rule::MiscStar,
                Rule::DoctypedeclMiscStarEroteme,
            ],
        );

        self.next()
    }

    /// Implementation of the optional [`XMLDecl` production
    /// rule](https://www.w3.org/TR/xml/#NT-XMLDecl), which can form
    /// part of the `prolog`.
    fn xml_decl_eroteme(&mut self) -> Option<Token> {
        self.remove_production_rule(Rule::XmlDeclEroteme);

        match XmlDecl::parse(&mut self.scanner) {
            Ok(Some(xml_decl)) => Some(Token::XmlDecl(xml_decl)),
            Ok(None) => self.next(),
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the `Misc*` production rule, which allows
    /// zero or more repetitions of the [`Misc` production
    /// rule](https://www.w3.org/TR/xml/#NT-Misc).
    fn misc_star(&mut self) -> Option<Token> {
        self.scanner.munch_whitespace();

        if let Some(token) = self.comment() {
            Some(token)
        } else if let Some(token) = self.pi() {
            Some(token)
        } else {
            self.remove_production_rule(Rule::MiscStar);

            self.next()
        }
    }

    /// Implementation of the [`Comment` production
    /// rule](https://www.w3.org/TR/xml/#NT-Comment).
    fn comment(&mut self) -> Option<Token> {
        match Comment::parse(&mut self.scanner) {
            Ok(Some(comment)) => Some(Token::Comment(comment)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`Pi` production
    /// rule](https://www.w3.org/TR/xml/#NT-PI).
    fn pi(&mut self) -> Option<Token> {
        match Pi::parse(&mut self.scanner) {
            Ok(Some(pi)) => Some(Token::ProcessingInstruction(pi)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the `(doctypedecl Misc*)?` production rule
    /// which forms part of the
    /// [`prolog`](https://www.w3.org/TR/xml/#NT-prolog).
    fn doctypedecl_misc_star_eroteme(&mut self) -> Option<Token> {
        self.replace_production_rule(Rule::DoctypedeclMiscStarEroteme, vec![Rule::MiscStar]);

        if let Some(token) = self.doctypedecl() {
            Some(token)
        } else {
            self.next()
        }
    }

    /// Implementation of the [`doctypedecl` production
    /// rule](https://www.w3.org/TR/xml/#NT-doctypedecl).
    fn doctypedecl(&mut self) -> Option<Token> {
        match DoctypeDecl::parse(&mut self.scanner) {
            Ok(Some(doctype_decl)) => Some(Token::DoctypeDecl(doctype_decl)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the root `element` rule within the top-level
    /// [`document` rule](https://www.w3.org/TR/xml/#document).
    fn root_element(&mut self) -> Token {
        self.remove_production_rule(Rule::RootElement);

        if let Some(root_element) = self.element() {
            root_element
        } else {
            self.error(Error::MissingRootElement)
        }
    }

    /// Implementation of the [`element`
    /// rule](https://www.w3.org/TR/xml/#NT-element).
    fn element(&mut self) -> Option<Token> {
        match EmptyOrSTag::parse(&mut self.scanner) {
            Ok(Some(EmptyOrSTag::EmptyTag(empty_tag))) => Some(Token::EmptyTag(empty_tag)),
            Ok(Some(EmptyOrSTag::STag(s_tag))) => {
                self.add_production_rule(Rule::Content);

                Some(Token::STag(s_tag))
            }
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`content`
    /// rule](https://www.w3.org/TR/xml/#NT-content).
    fn content(&mut self) -> Token {
        if let Some(cd_sect) = self.cd_sect() {
            return cd_sect;
        }

        if let Some(comment) = self.comment() {
            return comment;
        }

        if let Some(pi) = self.pi() {
            return pi;
        }

        if let Some(e_tag) = self.e_tag() {
            self.remove_production_rule(Rule::Content);

            return e_tag;
        }

        if let Some(element) = self.element() {
            return element;
        }

        if let Some(reference) = self.reference() {
            return reference;
        }

        if let Some(text) = self.text() {
            return text;
        }

        self.error(Error::BadState)
    }

    /// Implemntation of the [`CDSect`
    /// rule](https://www.w3.org/TR/xml/#NT-CDSect).
    fn cd_sect(&mut self) -> Option<Token> {
        match CdataSection::parse(&mut self.scanner) {
            Ok(Some(cdata_section)) => Some(Token::CdataSection(cdata_section)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Impelementation of the [`ETag`
    /// rule](https://www.w3.org/TR/xml/#NT-ETag).
    fn e_tag(&mut self) -> Option<Token> {
        match ETag::parse(&mut self.scanner) {
            Ok(Some(e_tag)) => Some(Token::ETag(e_tag)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`Reference`
    /// rule](https://www.w3.org/TR/xml/#NT-Reference).
    fn reference(&mut self) -> Option<Token> {
        if let Some(hex_char_ref) = self.hex_char_ref() {
            return Some(hex_char_ref);
        }

        if let Some(dec_char_ref) = self.dec_char_ref() {
            return Some(dec_char_ref);
        }

        if let Some(entity_ref) = self.entity_ref() {
            return Some(entity_ref);
        }

        None
    }

    /// Impelementation of the [`EntityRef`
    /// rule](https://www.w3.org/TR/xml/#NT-EntityRef).
    fn entity_ref(&mut self) -> Option<Token> {
        match EntityRef::parse(&mut self.scanner) {
            Ok(Some(entity_ref)) => Some(Token::EntityRef(entity_ref)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`CharRef` rule for decimal
    /// references](https://www.w3.org/TR/xml/#NT-CharRef).
    fn dec_char_ref(&mut self) -> Option<Token> {
        match DecCharRef::parse(&mut self.scanner) {
            Ok(Some(dec_char_ref)) => Some(Token::DecCharRef(dec_char_ref)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`CharRef` rule for hexidecimal
    /// references](https://www.w3.org/TR/xml/#NT-CharRef).
    fn hex_char_ref(&mut self) -> Option<Token> {
        match HexCharRef::parse(&mut self.scanner) {
            Ok(Some(hex_char_ref)) => Some(Token::HexCharRef(hex_char_ref)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Implementation of the [`CharData`
    /// rule](https://www.w3.org/TR/xml/#NT-CharData).
    fn text(&mut self) -> Option<Token> {
        match Text::parse(&mut self.scanner) {
            Ok(Some(text)) => Some(Token::Text(text)),
            Ok(None) => None,
            Err(error) => Some(self.error(error)),
        }
    }

    /// Add a production rule to the `rule_stack`. This will become the
    /// active rule on the next iteration.
    fn add_production_rule(&mut self, rule: Rule) {
        self.rule_stack.push(rule);
    }

    /// Replace the current production rule (which is expected to be
    /// `rule`) with the `replacement` rules.
    fn replace_production_rule(&mut self, rule: Rule, mut replacement: Vec<Rule>) {
        match self.rule_stack.pop() {
            Some(last) if last == rule => {
                replacement.reverse();
                self.rule_stack.append(&mut replacement);
            }
            _ => {
                panic!(
                    "Panic in replace_production_rule when attempting to replace {:?} with {:?}.",
                    rule, replacement
                );
            }
        }
    }

    /// Remove the current production rule which is expected to be
    /// `rule`.
    fn remove_production_rule(&mut self, rule: Rule) {
        match self.rule_stack.pop() {
            Some(popped) if popped == rule => (),
            Some(popped) => panic!(
                "In remove_production_rule: popped_rule={:?} unequal to expected={:?}",
                popped, rule
            ),
            None => panic!(
                "In remove_production_rule: rule_stack is empty, when expecting={:?}",
                rule
            ),
        }
    }

    /// Set the production rule to `Rule::Error` and return `error` as
    /// a token.
    fn error(&mut self, error: Error) -> Token {
        self.add_production_rule(Rule::Error);

        Token::Error(error)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::xml1_0::element::EmptyTag;
    use crate::NcName;
    use crate::QName;
    use std::convert::TryFrom;

    fn setup_tokenizer(markup: &str, rule_stack: Vec<Rule>) -> Tokenizer {
        let mut tokenizer = Tokenizer::new(markup.chars());
        tokenizer.rule_stack = rule_stack;

        tokenizer
    }

    #[test]
    fn document() {
        let mut tokenizer = setup_tokenizer("<?xml version='1.0'?><html/>", vec![Rule::Document]);
        assert_eq!(
            tokenizer.next(),
            Some(Token::XmlDecl(XmlDecl::new(None, None)))
        );
        assert_eq!(
            tokenizer.next(),
            Some(Token::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::try_from("html").unwrap()),
                vec![],
                vec![]
            )))
        );
    }

    #[test]
    fn root_element_only() {
        let mut tokenizer = setup_tokenizer("<html/>", vec![Rule::Document]);
        assert_eq!(
            tokenizer.next(),
            Some(Token::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::try_from("html").unwrap()),
                vec![],
                vec![]
            )))
        );
    }
}
