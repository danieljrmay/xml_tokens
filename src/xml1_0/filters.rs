use crate::xml1_0::reference::EntityRefMap;
use crate::xml1_0::token::Token;
use crate::xml1_0::CdataSection;
use crate::xml1_0::Comment;
use crate::xml1_0::Text;
use std::convert::TryFrom;

///  Removes `Comment` tokens, all other tokens pass through.
pub struct CommentRemover<'a> {
    input: &'a mut dyn Iterator<Item = Token>,
}
impl<'a> CommentRemover<'a> {
    pub fn new(input: &'a mut dyn Iterator<Item = Token>) -> CommentRemover<'a> {
        CommentRemover { input }
    }
}
impl<'a> Iterator for CommentRemover<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let opt_token = self.input.next();

        if let Some(Token::Comment(_)) = opt_token {
            self.next()
        } else {
            opt_token
        }
    }
}

pub enum ReferenceReplacerMode {
    ReplaceAllRefs,
    // TODO add ReplaceCharRefs,
    // TODO add ReplaceEntityRefs,
    NoAction,
}

/// Replace `EnityRef`, `DecCharRef` and `HexCharRef` in attributes
/// and text whereever possible. If not allowed due to the replacement
/// being a control character then a standard XML entity reference is
/// used e.g. `&amp;`, `&lt;`, `&quot;`.
pub struct ReferenceReplacer<'a> {
    input: &'a mut dyn Iterator<Item = Token>,
    entity_ref_map: &'a dyn EntityRefMap,
    mode: ReferenceReplacerMode,
}
impl<'a> ReferenceReplacer<'a> {
    pub fn new(
        input: &'a mut dyn Iterator<Item = Token>,
        entity_ref_map: &'a dyn EntityRefMap,
        mode: ReferenceReplacerMode,
    ) -> ReferenceReplacer<'a> {
        ReferenceReplacer {
            input,
            entity_ref_map,
            mode,
        }
    }
}
impl<'a> Iterator for ReferenceReplacer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let opt_token = self.input.next();

        if let ReferenceReplacerMode::NoAction = self.mode {
            return opt_token;
        }

        match opt_token {
            Some(Token::STag(mut s_tag)) => {
                s_tag.replace_references(self.entity_ref_map);

                Some(Token::STag(s_tag))
            }
            Some(Token::EmptyTag(mut empty_tag)) => {
                empty_tag.replace_references(self.entity_ref_map);

                Some(Token::EmptyTag(empty_tag))
            }
            Some(Token::EntityRef(entity_ref)) => match entity_ref.decode(self.entity_ref_map) {
                Ok(s) => match Text::new(s) {
                    Ok(text) => Some(Token::Text(text)),
                    Err(_) => Some(Token::EntityRef(entity_ref)),
                },
                Err(_) => Some(Token::EntityRef(entity_ref)),
            },
            Some(Token::DecCharRef(dec_char_ref)) => match Text::try_from(dec_char_ref.as_char()) {
                Ok(text) => Some(Token::Text(text)),
                Err(_) => Some(Token::DecCharRef(dec_char_ref)),
            },
            Some(Token::HexCharRef(hex_char_ref)) => match Text::try_from(hex_char_ref.as_char()) {
                Ok(text) => Some(Token::Text(text)),
                Err(_) => Some(Token::HexCharRef(hex_char_ref)),
            },
            _ => opt_token,
        }
    }
}

/// Merge neibouring `Text`, `Comments` and `CdataSection`s and
/// `AttValueData` variants together where possible. Useful after
/// reference replacements.
pub struct Merger<'a> {
    input: &'a mut dyn Iterator<Item = Token>,
    buffer: Vec<Token>,
}
impl<'a> Merger<'a> {
    pub fn new(input: &'a mut dyn Iterator<Item = Token>) -> Merger<'a> {
        Merger {
            input,
            buffer: Vec::new(),
        }
    }

    fn merge_comments(&mut self, mut comment: Comment) -> Option<Token> {
        loop {
            match self.input.next() {
                Some(Token::Comment(next_comment)) => {
                    if comment.merge(&next_comment).is_err() {
                        self.buffer.push(Token::Comment(next_comment));

                        return Some(Token::Comment(comment));
                    } else {
                        continue;
                    }
                }
                Some(token) => {
                    self.buffer.push(token);

                    return Some(Token::Comment(comment));
                }
                None => return Some(Token::Comment(comment)),
            }
        }
    }

    fn merge_cdata_sections(&mut self, mut cdata_section: CdataSection) -> Option<Token> {
        loop {
            match self.input.next() {
                Some(Token::CdataSection(next_cdata_section)) => {
                    if cdata_section.merge(&next_cdata_section).is_err() {
                        self.buffer.push(Token::CdataSection(next_cdata_section));

                        return Some(Token::CdataSection(cdata_section));
                    } else {
                        continue;
                    }
                }
                Some(token) => {
                    self.buffer.push(token);

                    return Some(Token::CdataSection(cdata_section));
                }
                None => return Some(Token::CdataSection(cdata_section)),
            }
        }
    }

    fn merge_texts(&mut self, mut text: Text) -> Option<Token> {
        loop {
            match self.input.next() {
                Some(Token::Text(next_text)) => {
                    if text.merge(&next_text).is_err() {
                        self.buffer.push(Token::Text(next_text));

                        return Some(Token::Text(text));
                    } else {
                        continue;
                    }
                }
                Some(token) => {
                    self.buffer.push(token);

                    return Some(Token::Text(text));
                }
                None => return Some(Token::Text(text)),
            }
        }
    }
}
impl<'a> Iterator for Merger<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(token) = self.buffer.pop() {
            return Some(token);
        }

        match self.input.next() {
            Some(Token::Comment(comment)) => self.merge_comments(comment),
            Some(Token::CdataSection(cdata_section)) => self.merge_cdata_sections(cdata_section),
            Some(Token::Text(text)) => self.merge_texts(text),
            Some(Token::EmptyTag(mut empty_tag)) => {
                empty_tag.merge_attribute_value_data();

                Some(Token::EmptyTag(empty_tag))
            }
            Some(Token::STag(mut s_tag)) => {
                s_tag.merge_attribute_value_data();

                Some(Token::STag(s_tag))
            }
            Some(token) => Some(token),
            None => None,
        }
    }
}

#[cfg(test)]
mod merger_tests {
    use super::*;
    use crate::xml1_0::doctype_decl::DoctypeDecl;
    use crate::xml1_0::tokenizer::Tokenizer;
    use crate::xml1_0::XmlDecl;
    use crate::Name;

    #[test]
    fn comments() {
        let mut tokenizer = Tokenizer::new(
            "<?xml version='1.0'?> <!--A--> <!--B--> <!--C--> <!DOCTYPE html><html/>".chars(),
        );
        let mut merger = Merger::new(&mut tokenizer);

        assert_eq!(
            merger.next(),
            Some(Token::XmlDecl(XmlDecl::new(None, None)))
        );
        assert_eq!(
            merger.next(),
            Some(Token::Comment(Comment::try_from("ABC").unwrap()))
        );
        assert_eq!(
            merger.next(),
            Some(Token::DoctypeDecl(DoctypeDecl::new(
                Name::try_from("html").unwrap(),
                None,
                None
            )))
        );
    }

    #[test]
    fn cdata_section() {
        let mut tokenizer = Tokenizer::new("<html><![CDATA[A]]><![CDATA[B]]><![CDATA[C]]>".chars());
        let mut merger = Merger::new(&mut tokenizer);

        assert!(merger.next().is_some());
        assert_eq!(
            merger.next(),
            Some(Token::CdataSection(CdataSection::try_from("ABC").unwrap()))
        );
    }
}

/// Deduplicate whitespace characters in `Text`, `Comment`,
/// `CdataSections` and attributes. Replaces neibouring whitespace
/// characters with a single space character.
pub struct WhitespaceDeduplicator<'a> {
    input: &'a mut dyn Iterator<Item = Token>,
}
impl<'a> WhitespaceDeduplicator<'a> {
    pub fn new(input: &'a mut dyn Iterator<Item = Token>) -> WhitespaceDeduplicator<'a> {
        WhitespaceDeduplicator { input }
    }
}
impl<'a> Iterator for WhitespaceDeduplicator<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let opt_token = self.input.next();

        match opt_token {
            Some(Token::STag(mut s_tag)) => {
                s_tag.deduplicate_whitespace();

                return Some(Token::STag(s_tag));
            }
            Some(Token::EmptyTag(mut empty_tag)) => {
                empty_tag.deduplicate_whitespace();

                return Some(Token::EmptyTag(empty_tag));
            }
            Some(Token::Comment(mut comment)) => {
                comment.deduplicate_whitespace();

                return Some(Token::Comment(comment));
            }
            Some(Token::CdataSection(mut cdata_section)) => {
                cdata_section.deduplicate_whitespace();

                return Some(Token::CdataSection(cdata_section));
            }
            Some(Token::Text(mut text)) => {
                text.deduplicate_whitespace();

                return Some(Token::Text(text));
            }
            _ => (),
        }

        opt_token
    }
}

/// Merges tokens and normalizes whitespace in attributes and
/// comments. It deduplicates whitespace in `Text` and
/// `CdataSections`.
pub struct WhitespaceReducer<'a> {
    merger: Merger<'a>,
}
impl<'a> WhitespaceReducer<'a> {
    pub fn new(input: &'a mut dyn Iterator<Item = Token>) -> WhitespaceReducer<'a> {
        WhitespaceReducer {
            merger: Merger::new(input),
        }
    }
}
impl<'a> Iterator for WhitespaceReducer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let opt_token = self.merger.next();

        match opt_token {
            Some(Token::CdataSection(mut cdata)) => {
                cdata.deduplicate_whitespace();
                Some(Token::CdataSection(cdata))
            }
            Some(Token::Text(mut text)) => {
                text.deduplicate_whitespace();
                Some(Token::Text(text))
            }
            Some(Token::Comment(mut comment)) => {
                comment.normalize_whitespace();
                Some(Token::Comment(comment))
            }
            Some(Token::STag(mut s_tag)) => {
                s_tag.normalize_whitespace();
                Some(Token::STag(s_tag))
            }
            Some(Token::EmptyTag(mut empty_tag)) => {
                empty_tag.normalize_whitespace();
                Some(Token::EmptyTag(empty_tag))
            }
            _ => opt_token,
        }
    }
}

/// Remove empty comments, texts and cdata sections.
pub struct EmptyTokenRemover<'a> {
    input: &'a mut dyn Iterator<Item = Token>,
}
impl<'a> EmptyTokenRemover<'a> {
    pub fn new(input: &'a mut dyn Iterator<Item = Token>) -> EmptyTokenRemover<'a> {
        EmptyTokenRemover { input }
    }
}
impl<'a> Iterator for EmptyTokenRemover<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let opt_token = self.input.next();

        match opt_token {
            Some(Token::Comment(comment)) if comment.is_empty() => self.next(),
            Some(Token::CdataSection(cdata_section)) if cdata_section.is_empty() => self.next(),
            Some(Token::Text(text)) if text.is_empty() => self.next(),
            _ => opt_token,
        }
    }
}
