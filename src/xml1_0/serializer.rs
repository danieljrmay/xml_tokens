use crate::xml1_0::token::Token;

/// Serialize a token iterator to markup.
///
/// # Example
///
/// ```
/// use xml_tokens::xml1_0::serializer::Serializer;
/// use xml_tokens::xml1_0::tokenizer::Tokenizer;
///
/// let mut tokenizer = Tokenizer::new("<?xml version='1.0'?> <!--Comment--> <root  />".chars());
/// let mut serializer = Serializer::new(&mut tokenizer);
/// let mut markup = String::new();
///
/// serializer.format(&mut markup);
///
/// assert_eq!(markup, "<?xml version=\"1.0\"?><!--Comment--><root/>");
/// ```
pub struct Serializer<'a> {
    token_iter: &'a mut dyn Iterator<Item = Token>,
}
impl<'a> Serializer<'a> {
    /// Create a new `Serializer` from a token iterator.
    pub fn new(token_iter: &'a mut dyn Iterator<Item = Token>) -> Serializer {
        Serializer { token_iter }
    }

    /// Serialize this `Serializer`'s token iterator to the writer `w`.
    pub fn format(&mut self, w: &mut dyn std::fmt::Write) -> Result<(), std::fmt::Error> {
        while let Some(token) = self.token_iter.next() {
            token.format(w)?;
        }

        Ok(())
    }

    /// Serialize this `Serializer`'s token iterator to the writer `w`.
    pub fn write(&mut self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        while let Some(token) = self.token_iter.next() {
            token.write(w)?;
        }

        Ok(())
    }
}
