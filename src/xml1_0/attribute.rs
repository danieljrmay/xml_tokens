use crate::deduplicate_whitespace;
use crate::normalize_whitespace;
use crate::normalize_whitespace_deduplicate_head;
use crate::normalize_whitespace_deduplicate_tail;
use crate::scanner::Scanner;
use crate::xml1_0::is_valid_char;
use crate::xml1_0::reference::DecCharRef;
use crate::xml1_0::reference::EntityRef;
use crate::xml1_0::reference::EntityRefMap;
use crate::xml1_0::reference::HexCharRef;
use crate::Error;
use crate::Name;
use crate::QName;
use crate::Quotes;
use std::fmt::Write;
use std::{convert::TryFrom, fmt};

/// Represents an Attribute. See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-Attribute) for more
/// details.
#[derive(Debug, PartialEq)]
pub struct Attribute {
    name: QName,
    value: AttValue,
}
impl Attribute {
    /// Create a new `Attribute`.
    pub fn new(name: QName, value: AttValue) -> Attribute {
        Attribute { name, value }
    }

    /// Return reference to the name of this `Attribute`.
    pub fn qname(&self) -> &QName {
        &self.name
    }

    /// Return reference to the value of this `Attribute`.
    pub fn value(&self) -> &AttValue {
        &self.value
    }

    /// Deduplicate whitespace characters in attribute
    /// values. Replaces neibouring whitespace characters with a
    /// single space character.
    pub fn deduplicate_whitespace(&mut self) {
        for att_value_data in self.value.data.iter_mut() {
            att_value_data.deduplicate_whitespace();
        }
    }

    /// Normalize whitespace characters in attribute values. Replaces
    /// neibouring whitespace characters with a single space
    /// character and trims whitespace from the head and tail.
    pub fn normalize_whitespace(&mut self) {
        let n = self.value.data.len();

        match n {
            0 => (),
            1 => self.value.data[0].normalize_whitespace(),
            _ => {
                self.value.data[0].normalize_whitespace_deduplicated_tail();

                for i in 1..(n - 1) {
                    self.value.data[i].deduplicate_whitespace();
                }

                self.value.data[n - 1].normalize_whitespace_deduplicated_head();
            }
        }
    }

    /// Merge `AttValueData::CharData` variants together where possible.
    pub fn merge(&mut self) {
        let mut new_data: Vec<AttValueData> = Vec::new();
        let mut s: String = String::new();

        self.value.data.reverse();

        loop {
            match self.value.data.pop() {
                Some(AttValueData::CharData(char_data)) => {
                    s.push_str(&char_data);
                }
                Some(att_value_data) => {
                    if !s.is_empty() {
                        new_data.push(AttValueData::CharData(s));
                        s = String::new();
                    }

                    new_data.push(att_value_data)
                }
                None => {
                    if !s.is_empty() {
                        new_data.push(AttValueData::CharData(s));
                    }

                    break;
                }
            }
        }

        self.value = AttValue::new(new_data);
    }

    /// Replace as many refernces as possible in this attributes value
    /// using the provided `EntityRefMap`.
    pub fn replace_references(&mut self, entity_ref_map: &dyn EntityRefMap) {
        let mut new_data: Vec<AttValueData> = Vec::new();

        for att_value_data in &self.value.data {
            new_data.push(att_value_data.replace_references(entity_ref_map));
        }

        self.value = AttValue::new(new_data);
    }

    /// Parse a `Attribute` using the provided `scanner`. If `Ok(None)`
    /// is returned then no characters have been consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<Attribute>, Error> {
        let qname = match QName::parse(scanner)? {
            Some(qname) => qname,
            None => return Ok(None),
        };

        scanner.munch_whitespace();
        scanner.munch_equals()?;
        scanner.munch_whitespace();

        let value = AttValue::parse(scanner)?;

        Ok(Some(Attribute::new(qname, value)))
    }

    /// Serialize the `Attribute` using the provided Writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, " {}=", self.name)?;
        self.value.format(w)
    }

    /// Write the `Attribute` using the provided Writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, " {}=", self.name)?;
        self.value.write(w)
    }
}

#[cfg(test)]
mod attribute_tests {
    use super::*;
    use crate::Name;
    use crate::NcName;
    use std::convert::TryFrom;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("prefix:local=\"Value\"".chars());
        assert_eq!(
            Attribute::parse(&mut scanner),
            Ok(Some(Attribute::new(
                QName::new(
                    Some(NcName::try_from("prefix").unwrap()),
                    NcName::try_from("local").unwrap()
                ),
                AttValue::new(vec![AttValueData::CharData(String::from("Value"))])
            )))
        );

        let mut scanner = Scanner::new(
            "prefix:local   =   'Value &amp; \"double quotes\" &#x2026; and &#65;'   ".chars(),
        );
        assert_eq!(
            Attribute::parse(&mut scanner),
            Ok(Some(Attribute::new(
                QName::new(
                    Some(NcName::try_from("prefix").unwrap()),
                    NcName::try_from("local").unwrap()
                ),
                AttValue::new(vec![
                    AttValueData::CharData(String::from("Value ")),
                    AttValueData::EntityRef(EntityRef::new_unchecked(
                        Name::try_from("amp").unwrap()
                    )),
                    AttValueData::CharData(String::from(" \"double quotes\" ")),
                    AttValueData::HexCharRef(HexCharRef::new_unchecked('…')),
                    AttValueData::CharData(String::from(" and ")),
                    AttValueData::DecCharRef(DecCharRef::new_unchecked('A')),
                ])
            )))
        );
    }
}

/// Represents an attribute value. Attribute values can consist of
/// character data as well as references. This requires `AttValue` to
/// be a container for a list of these differnt possible types. So the
/// internal representation of `AttValue` is a vector of
/// `AttValueData` items.
///
/// See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-AttValue) for more
/// details.
#[derive(Debug, PartialEq)]
pub struct AttValue {
    data: Vec<AttValueData>,
}
impl AttValue {
    /// Create a new `AttValue`.
    pub fn new(data: Vec<AttValueData>) -> AttValue {
        AttValue { data }
    }

    /// Parse a `Attribute` using the provided `scanner`. If `Ok(None)`
    /// is returned then no characters have been consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<AttValue, Error> {
        let enclosing_quotes = Quotes::parse(scanner)?;

        let mut data: Vec<AttValueData> = Vec::new();

        while let Some(att_value_data) = AttValueData::parse(scanner, enclosing_quotes)? {
            data.push(att_value_data);
        }

        Ok(AttValue::new(data))
    }

    /// Serialize the `AttValue` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        let mut value = String::new();

        for avd in self.data.iter() {
            avd.format(&mut value)?;
        }

        if value.contains('"') {
            if value.contains('\'') {
                write!(w, "'{}'", value.replace('\'', "&apos;"))
            } else {
                write!(w, "'{}'", value)
            }
        } else {
            write!(w, "\"{}\"", value)
        }
    }

    /// Write the `AttValue` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        let mut value = String::new();

        for avd in self.data.iter() {
            if let Err(error) = avd.format(&mut value) {
                return Err(std::io::Error::new(std::io::ErrorKind::Other, error));
            }
        }

        if value.contains('"') {
            if value.contains('\'') {
                write!(w, "'{}'", value.replace('\'', "&apos;"))
            } else {
                write!(w, "'{}'", value)
            }
        } else {
            write!(w, "\"{}\"", value)
        }
    }
}
impl fmt::Display for AttValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for avd in self.data.iter() {
            f.write_fmt(format_args!("{}", avd))?;
        }

        Ok(())
    }
}

/// Represents the different types of data that can appear in a
/// attribute value. There are variants for normal character data,
/// enity refernces, decimal character references and hexidecimal
/// character references.
#[derive(Debug, PartialEq)]
pub enum AttValueData {
    CharData(String),
    EntityRef(EntityRef),
    HexCharRef(HexCharRef),
    DecCharRef(DecCharRef),
}
impl AttValueData {
    /// Parse a `AttValueData` using the provided `scanner`. If
    /// `Ok(None)` is returned then the closing enclosing quote has
    /// just been consumed.
    pub fn parse(
        scanner: &mut Scanner,
        enclosing_quotes: Quotes,
    ) -> Result<Option<AttValueData>, Error> {
        if scanner.munch_quotes(enclosing_quotes) {
            Ok(None)
        } else if let Some(hex_char_ref) = HexCharRef::parse(scanner)? {
            Ok(Some(AttValueData::HexCharRef(hex_char_ref)))
        } else if let Some(dec_char_ref) = DecCharRef::parse(scanner)? {
            Ok(Some(AttValueData::DecCharRef(dec_char_ref)))
        } else if let Some(entity_ref) = EntityRef::parse(scanner)? {
            Ok(Some(AttValueData::EntityRef(entity_ref)))
        } else {
            match enclosing_quotes {
                Quotes::Double => {
                    if let Some(s) =
                        scanner.read_while(16, AttValueData::is_valid_double_quote_enclosed_char)
                    {
                        Ok(Some(AttValueData::CharData(s)))
                    } else {
                        Err(Error::IllegalCharacterInAttribute)
                    }
                }
                Quotes::Single => {
                    if let Some(s) =
                        scanner.read_while(16, AttValueData::is_valid_single_quote_enclosed_char)
                    {
                        Ok(Some(AttValueData::CharData(s)))
                    } else {
                        Err(Error::IllegalCharacterInAttribute)
                    }
                }
            }
        }
    }

    /// Deduplicate whitespace in attribute values.
    fn deduplicate_whitespace(&mut self) {
        if let AttValueData::CharData(char_data) = self {
            let deduplicated_char_data = deduplicate_whitespace(char_data);
            *char_data = deduplicated_char_data;
        }
    }

    /// Trim whitespace from head and tail and replace consecutive
    /// whitespace characters with a single space.
    pub fn normalize_whitespace(&mut self) {
        if let AttValueData::CharData(char_data) = self {
            let normalized_char_data = normalize_whitespace(char_data);
            *char_data = normalized_char_data;
        }
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the end.
    pub fn normalize_whitespace_deduplicated_head(&mut self) {
        if let AttValueData::CharData(char_data) = self {
            let new_char_data = normalize_whitespace_deduplicate_head(char_data);
            *char_data = new_char_data;
        }
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the beginning.
    pub fn normalize_whitespace_deduplicated_tail(&mut self) {
        if let AttValueData::CharData(char_data) = self {
            let new_char_data = normalize_whitespace_deduplicate_tail(char_data);
            *char_data = new_char_data;
        }
    }

    /// Replace `EnityRef`, `DecCharRef` and `HexCharRef` where
    /// possible. If not allowed due to the replacement being a
    /// control character then a standard XML entity reference is used
    /// e.g. `&amp;`, `&lt;`, `&quot;`.
    fn replace_references(&self, entity_ref_map: &dyn EntityRefMap) -> AttValueData {
        match self {
            AttValueData::CharData(char_data) => AttValueData::CharData(String::from(char_data)),
            AttValueData::EntityRef(entity_ref) => match entity_ref.decode(entity_ref_map) {
                Ok(s) => AttValueData::CharData(s),
                Err(_) => AttValueData::EntityRef(EntityRef::new_unchecked(Name::new_unchecked(
                    entity_ref.name().to_string(),
                ))),
            },
            AttValueData::HexCharRef(hex_char_ref) => match hex_char_ref.as_char() {
                '<' => {
                    AttValueData::EntityRef(EntityRef::new_unchecked(Name::try_from("lt").unwrap()))
                }
                '&' => AttValueData::EntityRef(EntityRef::new_unchecked(
                    Name::try_from("amp").unwrap(),
                )),
                '"' => AttValueData::EntityRef(EntityRef::new_unchecked(
                    Name::try_from("quot").unwrap(),
                )),
                c => AttValueData::CharData(String::from(c)),
            },
            AttValueData::DecCharRef(dec_char_ref) => match dec_char_ref.as_char() {
                '<' => {
                    AttValueData::EntityRef(EntityRef::new_unchecked(Name::try_from("lt").unwrap()))
                }
                '&' => AttValueData::EntityRef(EntityRef::new_unchecked(
                    Name::try_from("amp").unwrap(),
                )),
                '"' => AttValueData::EntityRef(EntityRef::new_unchecked(
                    Name::try_from("quot").unwrap(),
                )),
                c => AttValueData::CharData(String::from(c)),
            },
        }
    }

    /// Returns `true` if `c` is a valid character to be included in
    /// `AttValueData::CharData` when the attribute is enclosed in
    /// double quotes.
    fn is_valid_double_quote_enclosed_char(c: char) -> bool {
        if matches!(c, '<' | '&' | '"') {
            return false;
        }

        is_valid_char(c)
    }

    /// Returns `true` if `c` is a valid character to be included in
    /// `AttValueData::CharData` when the attribute is enclosed in
    /// single quotes.
    fn is_valid_single_quote_enclosed_char(c: char) -> bool {
        if matches!(c, '<' | '&' | '\'') {
            return false;
        }

        is_valid_char(c)
    }

    /// Serialize the `AttValueData` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        match self {
            AttValueData::CharData(s) => write!(w, "{}", s),
            AttValueData::EntityRef(entity_ref) => entity_ref.format(w),
            AttValueData::DecCharRef(dec_char_ref) => dec_char_ref.format(w),
            AttValueData::HexCharRef(hex_char_ref) => hex_char_ref.format(w),
        }
    }

    /// Write the `AttValueData` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        match self {
            AttValueData::CharData(s) => write!(w, "{}", s),
            AttValueData::EntityRef(entity_ref) => entity_ref.write(w),
            AttValueData::DecCharRef(dec_char_ref) => dec_char_ref.write(w),
            AttValueData::HexCharRef(hex_char_ref) => hex_char_ref.write(w),
        }
    }
}
impl fmt::Display for AttValueData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AttValueData::CharData(s) => write!(f, "{}", &s),
            AttValueData::EntityRef(entity_ref) => write!(f, "{}", entity_ref),
            AttValueData::DecCharRef(dec_char_ref) => write!(f, "{}", dec_char_ref),
            AttValueData::HexCharRef(hex_char_ref) => write!(f, "{}", hex_char_ref),
        }
    }
}
