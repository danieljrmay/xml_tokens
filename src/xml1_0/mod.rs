/// Attribute related code.
pub mod attribute;

/// Representation of a doctype declaration e.g. `<!DOCTYPE ... >`.
pub mod doctype_decl;

/// Representations of element tags e.g. `<s_tag>`, `<empty_tag/>` or `</e_tag>`.
pub mod element;

/// Various token filters.
pub mod filters;

/// Representation of namespace declarations and values.
pub mod namespace;

/// Representation of a processing instruction e.g. `<?php my data?>`
pub mod pi;

/// Representations of the various character references of XML 1.0.
pub mod reference;

/// Serializes tokens to XML 1.0 markup.
pub mod serializer;

/// Tokens to represent the "atomic" parts of XML 1.0 markup.
pub mod token;

/// Parses XML markup into tokens.
pub mod tokenizer;

/// Returns `true` if `c` is a valid XML 1.0 character. See the [XML
/// 1.0 specification](https://www.w3.org/TR/xml/#charsets) for more
/// details.
pub fn is_valid_char(c: char) -> bool {
    matches!(c,
             '\u{9}' | '\u{A}' | '\u{D}' |
             '\u{20}'..='\u{D7FF}' |
             '\u{E000}'..='\u{FFFD}' |
             '\u{10000}'..='\u{10FFFF}')
}

/// Returns `true` if `c` is a valid but *discouraged* XML 1.0 character. See the [XML
/// 1.0 specification](https://www.w3.org/TR/xml/#charsets) for more
/// details.
pub fn is_discouraged_char(c: char) -> bool {
    matches!(c,
             '\u{7F}'..='\u{84}' | '\u{86}'..='\u{9F}' | '\u{FDD0}'..='\u{FDEF}' |
             '\u{1FFFE}'..='\u{1FFFF}' | '\u{2FFFE}'..='\u{2FFFF}' | '\u{3FFFE}'..='\u{3FFFF}' |
             '\u{4FFFE}'..='\u{4FFFF}' | '\u{5FFFE}'..='\u{5FFFF}' | '\u{6FFFE}'..='\u{6FFFF}' |
             '\u{7FFFE}'..='\u{7FFFF}' | '\u{8FFFE}'..='\u{8FFFF}' | '\u{9FFFE}'..='\u{9FFFF}' |
             '\u{AFFFE}'..='\u{AFFFF}' | '\u{BFFFE}'..='\u{BFFFF}' | '\u{CFFFE}'..='\u{CFFFF}' |
             '\u{DFFFE}'..='\u{DFFFF}' | '\u{EFFFE}'..='\u{EFFFF}' | '\u{FFFFE}'..='\u{FFFFF}' |
             '\u{10FFFE}'..='\u{10FFFF}')
}

use crate::deduplicate_whitespace;
use crate::is_whitespace;
use crate::normalize_whitespace;
use crate::normalize_whitespace_deduplicate_head;
use crate::normalize_whitespace_deduplicate_tail;
use crate::scanner::Scanner;
use crate::Error;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::Write;

/// A `CDATASection` represents markup of the form `<![CDATA[My cdata]]>`.
///
/// The [XML 1.0 specification](https://www.w3.org/TR/xml/#NT-CData)
/// defines a `CdataSection` as containing any legal XML character
/// with the exception of the "]]>" terminiating delimiter.
#[derive(Debug, PartialEq)]
pub struct CdataSection {
    data: String,
}
impl CdataSection {
    /// Create a `CdataSection` checking that it conforms to the [XML
    /// 1.0 specification](https://www.w3.org/TR/xml/#NT-CData). Bascically
    /// `data` can contain zero or more legal XML 1.0 characters with the
    /// exception of the "]]>" CdataSection terminiating delimiter.
    pub fn new(data: String) -> Result<CdataSection, Error> {
        if !data.chars().all(is_valid_char) {
            return Err(Error::IllegalCharacterInCdataSection);
        }

        if data.contains("]]>") {
            return Err(Error::CdataSectionEndInCdataSection);
        }

        Ok(CdataSection::new_unchecked(data))
    }

    /// Create a `CdataSection` *without* checking that it conforms to
    /// the [XML 1.0
    /// specification](https://www.w3.org/TR/xml/#NT-CData). Bascically
    /// `data` can contain zero or more legal XML characters with the
    /// exception of the "]]>" CdataSection terminiating delimiter.
    pub fn new_unchecked(data: String) -> CdataSection {
        CdataSection { data }
    }

    /// Return `true` if this `CdataSection` is empty.
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }

    /// Attempt to create a new `CdataSection` by concatenating `other` on
    /// to the end of this `CdataSection`.
    pub fn merge(&mut self, other: &CdataSection) -> Result<(), Error> {
        let mut s = String::from(&self.data);
        s.push_str(&other.data);

        if s.contains("]]>") {
            return Err(Error::CdataSectionEndInCdataSection);
        }

        self.data = s;

        Ok(())
    }

    /// Replace consecutive whitespace characters with a single
    /// space.
    pub fn deduplicate_whitespace(&mut self) {
        let s = deduplicate_whitespace(&self.data);
        self.data = s;
    }

    /// Trim whitespace from head and tail and replace consecutive
    /// whitespace characters with a single space.
    pub fn normalize_whitespace(&mut self) {
        let s = normalize_whitespace(&self.data);
        self.data = s;
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the end.
    pub fn normalize_whitespace_deduplicated_head(&mut self) {
        let s = normalize_whitespace_deduplicate_head(&self.data);
        self.data = s;
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the beginning.
    pub fn normalize_whitespace_deduplicated_tail(&mut self) {
        let s = normalize_whitespace_deduplicate_tail(&self.data);
        self.data = s;
    }

    /// Test if the first character is a whitespace.
    pub fn is_first_character_whitespace(&self) -> bool {
        if let Some(c) = self.data.chars().next() {
            return is_whitespace(c);
        }

        false
    }

    /// Test if the last character is a whitespace.
    pub fn is_last_character_whitespace(&self) -> bool {
        if let Some(c) = self.data.chars().last() {
            return is_whitespace(c);
        }

        false
    }

    /// Parse a `CdataSection` using the provided `scanner`. The
    /// CdataSection terminiating sequence is consumed upon
    /// success. If Ok(None) is returned then nothing has been
    /// consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<CdataSection>, Error> {
        if !scanner.munch_cdata_section_start() {
            return Ok(None);
        }

        let data =
            scanner.read_while_until_terminating_sequence(64, is_valid_char, &[']', ']', '>'])?;

        Ok(Some(CdataSection::new_unchecked(data)))
    }

    /// Serialize the `CdataSection` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<![CDATA[{}]]>", self)
    }

    /// Write the `CdataSection` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<![CDATA[{}]]>", self)
    }
}
impl fmt::Display for CdataSection {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.data)
    }
}
impl TryFrom<&str> for CdataSection {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod cdata_section_tests {
    use super::*;

    #[test]
    fn new() {
        let result = CdataSection::new(String::from("Legal cdata section."));
        assert!(result.is_ok());

        let result = CdataSection::new(String::from("Illegal \0 cdata section."));
        assert!(result.is_err());

        let result = CdataSection::new(String::from("Illegal ]]> cdata section."));
        assert!(result.is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("<![CDATA[Legal cdata section.]]>".chars());
        assert_eq!(
            CdataSection::parse(&mut scanner),
            Ok(Some(
                CdataSection::try_from("Legal cdata section.").unwrap()
            ))
        );

        let mut scanner = Scanner::new(
            " <![CDATA[Legal cdata section not started yet (leading whitespace).]]>".chars(),
        );
        assert_eq!(CdataSection::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<![CDATA[Illegal \0 cdata section.]]>".chars());
        assert_eq!(
            CdataSection::parse(&mut scanner),
            Err(Error::IllegalCharacter)
        );

        let mut scanner =
            Scanner::new("<![CDATA[Illegal cdata section, missing terminator.".chars());
        assert_eq!(
            CdataSection::parse(&mut scanner),
            Err(Error::MissingTerminatingSequence)
        );
    }
}

/// Represents an XML 1.0 comment e.g. `<!--Comment-->`.
///
/// See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-Comment) for
/// details. Note that an XML 1.0 comment is not necessarily the same
/// as a XML 1.1 comment. The two specificaitons allow different
/// characters.
#[derive(Debug, PartialEq)]
pub struct Comment {
    comment: String,
}
impl Comment {
    /// Create a `Comment` checking that it conforms to the [XML 1.0
    /// specification](https://www.w3.org/TR/xml/#NT-Comment).
    pub fn new(comment: String) -> Result<Comment, Error> {
        if !comment.chars().all(is_valid_char) {
            return Err(Error::IllegalCharacterInComment);
        }

        if comment.contains("--") {
            return Err(Error::TwoHyphensInComment);
        }

        Ok(Comment::new_unchecked(comment))
    }

    /// Create a `Comment` *without* checking that it conforms to the
    /// [XML 1.0
    /// specification](https://www.w3.org/TR/xml/#NT-Comment).
    pub fn new_unchecked(comment: String) -> Comment {
        Comment { comment }
    }

    /// Return `true` if this `Comment` is empty.
    pub fn is_empty(&self) -> bool {
        self.comment.is_empty()
    }

    /// Attempt to create a new `Comment` by concatenating `other` on
    /// to the end of this `Comment`.
    pub fn merge(&mut self, other: &Comment) -> Result<(), Error> {
        let mut s = String::from(&self.comment);
        s.push_str(&other.comment);

        if s.contains("--") {
            return Err(Error::TwoHyphensInComment);
        }

        self.comment = s;

        Ok(())
    }

    /// Replace consecutive whitespace characters with a single
    /// space.
    pub fn deduplicate_whitespace(&mut self) {
        let s = deduplicate_whitespace(&self.comment);
        self.comment = s;
    }

    /// Trim whitespace from head and tail and replace consecutive
    /// whitespace characters with a single space.
    pub fn normalize_whitespace(&mut self) {
        let s = normalize_whitespace(&self.comment);
        self.comment = s;
    }

    /// Parse a `Comment` using the provided `scanner`. If `Ok(None)`
    /// is returned then no characters have been consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<Comment>, Error> {
        if !scanner.munch_comment_start() {
            return Ok(None);
        }

        let mut comment = String::with_capacity(64);

        loop {
            if scanner.munch_comment_end() {
                break;
            }

            if scanner.munch_sequence(&['-', '-']) {
                return Err(Error::TwoHyphensInComment);
            }

            match scanner.next() {
                Some(c) if is_valid_char(c) => comment.push(c),
                Some(_c) => return Err(Error::IllegalCharacterInComment),
                None => return Err(Error::MissingCommentEnd(scanner.index())),
            }
        }

        comment.shrink_to_fit();

        Ok(Some(Comment::new_unchecked(comment)))
    }

    /// Serialize the `Comment` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<!--{}-->", self)
    }

    /// Write the `Comment` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<!--{}-->", self)
    }
}
impl fmt::Display for Comment {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.comment)
    }
}
impl TryFrom<&str> for Comment {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod comment_tests {
    use super::*;

    #[test]
    fn new() {
        let result = Comment::new(String::from("Legal comment."));
        assert!(result.is_ok());

        let result = Comment::new(String::new());
        assert!(result.is_ok());

        let result = Comment::new(String::from("Illegal \0 comment."));
        assert!(result.is_err());

        let result = Comment::new(String::from("Illegal -- comment."));
        assert!(result.is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("<!--Legal comment.-->".chars());
        assert_eq!(
            Comment::parse(&mut scanner),
            Ok(Some(Comment::try_from("Legal comment.").unwrap()))
        );

        let mut scanner = Scanner::new(" <!-- Legal comment with leading whitespace.-->".chars());
        assert_eq!(Comment::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<!--Illegal \0 character.-->".chars());
        assert_eq!(
            Comment::parse(&mut scanner),
            Err(Error::IllegalCharacterInComment)
        );

        let mut scanner = Scanner::new("<!--Legal comment, missing terminator.".chars());
        assert_eq!(
            Comment::parse(&mut scanner),
            Err(Error::MissingCommentEnd(38))
        );
    }
}

use crate::xml1_0::reference::DecCharRef;
use crate::xml1_0::reference::HexCharRef;

/// A `Text` contains character data.
///
/// See the [XML 1.0
/// specification](https://www.w3.org/TR/xml11/#dt-chardata) states
/// that a `Text` can contain any legal XML character with the
/// following exceptions:
///
/// * The `'<'` character is illegal, and must be represented by a character reference.
/// * The `'&'` character is illegal, and must be represented by a character reference.
/// * The CDATA-section-close delimiter `"]]>"` is illegal.
#[derive(Debug, PartialEq)]
pub struct Text {
    text: String,
}
impl Text {
    /// Create a new `Text` checking that it conforms to the [XML 1.0
    /// specification](https://www.w3.org/TR/xml11/#dt-chardata). Essentially
    /// `text` must be a legal XML 1.0 character excluding `'<'`, `'&'`
    /// and `"]]>"`.
    pub fn new(text: String) -> Result<Text, Error> {
        let mut iter = text.chars();

        if !iter.all(Text::is_valid_char) {
            return Err(Error::IllegalCharacterInText);
        }

        if text.contains("]]>") {
            return Err(Error::CdataSectionEndInText);
        }

        Ok(Text::new_unchecked(text))
    }

    /// Create a `Text` without checking that it conforms to the [XML
    /// 1.0 specification](https://www.w3.org/TR/xml11/#dt-chardata).
    pub fn new_unchecked(text: String) -> Text {
        Text { text }
    }

    /// Returns `true` if `c` is a legal `Text` character according to the [XML 1.0
    /// specification](https://www.w3.org/TR/xml11/#dt-chardata)
    pub fn is_valid_char(c: char) -> bool {
        if matches!(c, '<' | '&') {
            return false;
        }

        crate::xml1_0::is_valid_char(c)
    }

    /// Replace consecutive whitespace characters with a single
    /// space.
    pub fn deduplicate_whitespace(&mut self) {
        let s = deduplicate_whitespace(&self.text);
        self.text = s;
    }

    /// Trim whitespace from head and tail and replace consecutive
    /// whitespace characters with a single space.
    pub fn normalize_whitespace(&mut self) {
        let s = normalize_whitespace(&self.text);
        self.text = s;
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the end.
    pub fn normalize_whitespace_deduplicated_head(&mut self) {
        let s = normalize_whitespace_deduplicate_head(&self.text);
        self.text = s;
    }

    /// Replace consecutive whitespace characters with a single
    /// space, and trim all whitespace from the beginning.
    pub fn normalize_whitespace_deduplicated_tail(&mut self) {
        let s = normalize_whitespace_deduplicate_tail(&self.text);
        self.text = s;
    }

    /// Returns `true` if the `Text` is empty.
    pub fn is_empty(&self) -> bool {
        self.text.is_empty()
    }

    /// Returns the length of the `Text`.
    pub fn len(&self) -> usize {
        self.text.len()
    }

    /// Test if the first character is a whitespace.
    pub fn is_first_character_whitespace(&self) -> bool {
        if let Some(c) = self.text.chars().next() {
            return is_whitespace(c);
        }

        false
    }

    /// Test if the last character is a whitespace.
    pub fn is_last_character_whitespace(&self) -> bool {
        if let Some(c) = self.text.chars().last() {
            return is_whitespace(c);
        }

        false
    }

    /// Attempt to create a new `Text` by concatenating `other` on
    /// to the end of this `Text`.
    pub fn merge(&mut self, other: &Text) -> Result<(), Error> {
        let mut s = String::from(&self.text);
        s.push_str(&other.text);

        if s.contains("]]>") {
            return Err(Error::CdataSectionEndInText);
        }

        self.text = s;

        Ok(())
    }

    /// Parse a `Text` using the provided `scanner`. If `Ok(None)` is
    /// returned then no characters have been consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<Text>, Error> {
        let mut s: String = String::with_capacity(16);

        loop {
            if scanner.munch_sequence(&[']', ']', '>']) {
                return Err(Error::CdataSectionEndInText);
            }

            match scanner.next_if(Text::is_valid_char) {
                Some(c) => s.push(c),
                None => break,
            }
        }

        if s.is_empty() {
            Ok(None)
        } else {
            s.shrink_to_fit();

            Ok(Some(Text::new_unchecked(s)))
        }
    }

    /// Serialize the `Text` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "{}", self.text)
    }

    /// Write the `Text` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "{}", self.text)
    }
}
impl fmt::Display for Text {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.text)
    }
}
impl TryFrom<char> for Text {
    type Error = Error;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        Self::new(String::from(c))
    }
}
impl TryFrom<&str> for Text {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}
impl TryFrom<DecCharRef> for Text {
    type Error = Error;

    fn try_from(dec_char_ref: DecCharRef) -> Result<Self, Self::Error> {
        let c = dec_char_ref.as_char();

        if Text::is_valid_char(c) {
            Ok(Text::new_unchecked(String::from(c)))
        } else {
            Err(Error::IllegalCharacterInText)
        }
    }
}
impl TryFrom<HexCharRef> for Text {
    type Error = Error;

    fn try_from(hex_char_ref: HexCharRef) -> Result<Self, Self::Error> {
        let c = hex_char_ref.as_char();

        if Text::is_valid_char(c) {
            Ok(Text::new_unchecked(String::from(c)))
        } else {
            Err(Error::IllegalCharacterInText)
        }
    }
}

#[cfg(test)]
mod text_tests {
    use super::*;

    #[test]
    fn new() {
        let result = Text::new(String::from("Legal text."));
        assert!(result.is_ok());

        let result = Text::new(String::new());
        assert!(result.is_ok());

        let result = Text::new(String::from("Illegal \0 text."));
        assert!(result.is_err());

        let result = Text::new(String::from("Illegal ]]> text."));
        assert!(result.is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("Legal text.".chars());
        assert_eq!(
            Text::parse(&mut scanner),
            Ok(Some(Text::try_from("Legal text.").unwrap()))
        );

        let mut scanner = Scanner::new("".chars());
        assert_eq!(Text::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("&amp;".chars());
        assert_eq!(Text::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<!--Comment-->".chars());
        assert_eq!(Text::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("Legal text up to illegal character:\0.".chars());
        assert_eq!(
            Text::parse(&mut scanner),
            Ok(Some(
                Text::try_from("Legal text up to illegal character:").unwrap()
            ))
        );

        let mut scanner = Scanner::new("Illegal ]]> text.".chars());
        assert_eq!(Text::parse(&mut scanner), Err(Error::CdataSectionEndInText));
    }
}

use crate::EncName;
use crate::Quotes;

/// Represents an XML 1.0 declaration, e.g. `<?xml version="1.0" encoding="utf-8" standalone="no"?>`.
///
/// See the [XML 1.0](https://www.w3.org/TR/xml/#NT-XMLDecl) specification for details.
#[derive(Debug, PartialEq)]
pub struct XmlDecl {
    encoding: Option<EncName>,
    standalone: Option<bool>,
}
impl XmlDecl {
    /// Create a new XML 1.0 `XmlDecl`.
    pub fn new(encoding: Option<EncName>, standalone: Option<bool>) -> XmlDecl {
        XmlDecl {
            encoding,
            standalone,
        }
    }

    /// Get a reference to this `XmlDecl`'s encoding value.
    pub fn encoding(&self) -> &Option<EncName> {
        &self.encoding
    }

    /// Get a reference to this `XmlDecl`'s standalone value.
    pub fn standalone(&self) -> &Option<bool> {
        &self.standalone
    }

    /// Parse an XML 1.0 `XmlDecl` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<XmlDecl>, Error> {
        if !scanner.munch_xml_decl_start() {
            return Ok(None);
        }

        scanner.require_whitespace()?;
        scanner.munch_version()?;
        scanner.munch_whitespace();
        scanner.munch_equals()?;
        scanner.munch_whitespace();

        let version_open_quote = Quotes::parse(scanner)?;

        if !scanner.munch_one_point_zero() {
            if scanner.munch_one_point_one() {
                return Err(Error::XmlOnePointOneDetected(scanner.index()));
            } else {
                return Err(Error::IllegalXmlVersionNumber);
            }
        }

        let version_close_quote = Quotes::parse(scanner)?;

        if version_open_quote != version_close_quote {
            return Err(Error::UnmatchingQuotes(scanner.index()));
        }

        if !scanner.munch_whitespace() {
            scanner.munch_xml_decl_end()?;
            return Ok(Some(XmlDecl::new(None, None)));
        }

        let encoding = Self::parse_encoding(scanner)?;

        if encoding.is_some() && !scanner.munch_whitespace() {
            scanner.munch_xml_decl_end()?;
            return Ok(Some(XmlDecl::new(encoding, None)));
        }

        let standalone = Self::parse_standalone(scanner)?;

        if standalone.is_some() {
            scanner.munch_whitespace();
        }

        scanner.munch_xml_decl_end()?;

        Ok(Some(XmlDecl::new(encoding, standalone)))
    }

    /// Parse the encoding part of an XML declaration.
    fn parse_encoding(scanner: &mut Scanner) -> Result<Option<EncName>, Error> {
        if !scanner.munch_encoding() {
            return Ok(None);
        }

        scanner.munch_whitespace();
        scanner.munch_equals()?;
        scanner.munch_whitespace();

        let open_quotes: Quotes = Quotes::parse(scanner)?;
        let enc_name: EncName = EncName::parse(scanner)?;
        let close_quotes: Quotes = Quotes::parse(scanner)?;

        if open_quotes == close_quotes {
            Ok(Some(enc_name))
        } else {
            Err(Error::UnmatchingQuotes(scanner.index()))
        }
    }

    /// Parse the standalone part of an XML declaration.
    fn parse_standalone(scanner: &mut Scanner) -> Result<Option<bool>, Error> {
        if !scanner.munch_standalone() {
            return Ok(None);
        }

        scanner.munch_whitespace();
        scanner.munch_equals()?;
        scanner.munch_whitespace();

        Ok(Some(scanner.munch_standalone_value()?))
    }

    /// Serialize this `XmlDecl` to the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<?xml version=\"1.0\"")?;

        if let Some(enc_name) = &self.encoding {
            write!(w, " encoding=\"{}\"", enc_name)?;
        }

        if let Some(standalone) = self.standalone {
            write!(w, " standalone=\"{}\"", standalone)?;
        }

        write!(w, "?>")
    }

    /// Write this `XmlDecl` to the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<?xml version=\"1.0\"")?;

        if let Some(enc_name) = &self.encoding {
            write!(w, " encoding=\"{}\"", enc_name)?;
        }

        if let Some(standalone) = self.standalone {
            write!(w, " standalone=\"{}\"", standalone)?;
        }

        write!(w, "?>")
    }
}

#[cfg(test)]
mod xml_decl_tests {
    use super::*;
    use std::convert::TryFrom;

    #[test]
    fn parse() {
        // let mut scanner = Scanner::new("<?xml version=\"1.0\"?>".chars());
        // assert_eq!(
        //     XmlDecl::parse(&mut scanner),
        //     Ok(Some(XmlDecl::new(None, None)))
        // );

        // let mut scanner = Scanner::new("<?xml   version   =   \"1.0\" encoding='utf-8' ?>".chars());
        // assert_eq!(
        //     XmlDecl::parse(&mut scanner),
        //     Ok(Some(XmlDecl::new(
        //         Some(EncName::try_from("utf-8").unwrap()),
        //         None
        //     )))
        // );

        // let mut scanner = Scanner::new("<?xml versionX = \"1.0\" encoding='utf-8'?>".chars());
        // assert!(XmlDecl::parse(&mut scanner).is_err());

        // let mut scanner = Scanner::new(" <?xml   version   =   \"1.0\" ?>".chars());
        // assert_eq!(XmlDecl::parse(&mut scanner), Ok(None));

        // let mut scanner = Scanner::new("<?xml version=\"1.0\"encoding='utf-8'?>".chars());
        // assert!(XmlDecl::parse(&mut scanner).is_err());

        let mut scanner =
            Scanner::new("<?xml version=\"1.0\" standalone \n = \r 'yes'  ?>".chars());
        assert_eq!(
            XmlDecl::parse(&mut scanner),
            Ok(Some(XmlDecl::new(None, Some(true))))
        );
    }

    #[test]
    fn parse_encoding() {
        let mut scanner = Scanner::new("encoding='utf-8'".chars());
        assert_eq!(
            XmlDecl::parse_encoding(&mut scanner),
            Ok(Some(EncName::try_from("utf-8").unwrap()))
        );

        let mut scanner = Scanner::new("encoding      =        'iso8859-1'      ".chars());
        assert_eq!(
            XmlDecl::parse_encoding(&mut scanner),
            Ok(Some(EncName::try_from("iso8859-1").unwrap()))
        );

        let mut scanner = Scanner::new("encodingX = 'iso8859-1'".chars());
        assert!(XmlDecl::parse_encoding(&mut scanner).is_err());

        let mut scanner = Scanner::new("encoding='utf-8'?>".chars());
        assert_eq!(
            XmlDecl::parse_encoding(&mut scanner),
            Ok(Some(EncName::try_from("utf-8").unwrap()))
        );
        assert!(scanner.munch('?'));
        assert!(scanner.munch('>'));
    }

    #[test]
    fn parse_standalone() {
        let mut scanner = Scanner::new("standalone='yes'".chars());
        assert_eq!(XmlDecl::parse_standalone(&mut scanner), Ok(Some(true)));

        let mut scanner = Scanner::new("standalone   =    \"yes\"    ".chars());
        assert_eq!(XmlDecl::parse_standalone(&mut scanner), Ok(Some(true)));

        let mut scanner = Scanner::new("standalone    =     \"no\"       ?>".chars());
        assert_eq!(XmlDecl::parse_standalone(&mut scanner), Ok(Some(false)));
        assert!(scanner.munch_whitespace());
        assert!(scanner.munch('?'));
        assert!(scanner.munch('>'));

        let mut scanner = Scanner::new("?>".chars());
        assert_eq!(XmlDecl::parse_standalone(&mut scanner), Ok(None));
        assert!(scanner.munch('?'));
        assert!(scanner.munch('>'));

        let mut scanner = Scanner::new("    ?>".chars());
        assert_eq!(XmlDecl::parse_standalone(&mut scanner), Ok(None));
        assert!(scanner.munch_whitespace());
        assert_eq!(scanner.next(), Some('?'));
        assert_eq!(scanner.next(), Some('>'));
        assert_eq!(scanner.next(), None);
    }
}
