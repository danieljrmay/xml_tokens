<!-- markdownlint-disable MD033 -->
# <img src="doc/images/logo.png" width="100" alt="Logo"/> XML Tokens

[![pipeline
status](https://gitlab.com/danieljrmay/xml_tokens/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/xml_tokens/commits/master)

Parse XML to tokens. Serialize tokens to XML.
