use crate::scanner::Scanner;
use crate::xml1_0::is_valid_char;
use crate::Error;
use std::fmt;
use std::fmt::Write;

/// A `DecCharRef` corresponds to markup of the form `&#65;` and
/// represents a character.
///
/// The [XML 1.0 specification](https://www.w3.org/TR/xml/#NT-CharRef)
/// requires that any `DecCharRef` must correspond to a valid XML 1.0
/// character.
#[derive(Debug, PartialEq)]
pub struct DecCharRef {
    character: char,
}
impl DecCharRef {
    /// Create a `DecCharRef` checking that the supplied character is
    /// a valid [XML 1.0](https://www.w3.org/TR/xml/#NT-CharRef)
    /// character.
    pub fn new(c: char) -> Result<DecCharRef, Error> {
        if is_valid_char(c) {
            Ok(DecCharRef::new_unchecked(c))
        } else {
            Err(Error::IllegalCharacterInDecCharRef)
        }
    }

    /// Create a `DecCharRef` *without* checking that the supplied
    /// character is a valid XML 1.0 character.
    pub fn new_unchecked(c: char) -> DecCharRef {
        DecCharRef { character: c }
    }

    /// Return the character as a `char` value.
    pub fn as_char(&self) -> char {
        self.character
    }

    /// Return the character as a `u32` value.
    pub fn as_u32(&self) -> u32 {
        self.character as u32
    }

    /// Parse a `DecCharRef` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<DecCharRef>, Error> {
        if !scanner.munch_dec_char_ref_start() {
            return Ok(None);
        }

        let u32_value = scanner.read_decimal()?;
        scanner.munch_ref_end()?;

        if let Some(c) = std::char::from_u32(u32_value) {
            Ok(Some(DecCharRef::new(c)?))
        } else {
            Err(Error::IllegalUnicodeValue)
        }
    }

    /// Serialize the `DecCharRef` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "&#{};", self.as_u32())
    }

    /// Write the `DecCharRef` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "&#{};", self.as_u32())
    }
}
impl fmt::Display for DecCharRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.character)
    }
}

#[cfg(test)]
mod dec_char_ref_tests {
    use super::*;

    #[test]
    fn new() {
        let result = DecCharRef::new('A');
        assert!(result.is_ok());

        let dec_char_ref = result.unwrap();
        assert_eq!(dec_char_ref.as_char(), 'A');
        assert_eq!(dec_char_ref.as_u32(), 65);

        let result = DecCharRef::new('\0');
        assert!(result.is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("&#65;".chars());
        assert_eq!(
            DecCharRef::parse(&mut scanner),
            Ok(Some(DecCharRef::new_unchecked('A')))
        );

        let mut scanner = Scanner::new(" &#65;".chars());
        assert_eq!(DecCharRef::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("&#000;".chars());
        assert_eq!(
            DecCharRef::parse(&mut scanner),
            Err(Error::IllegalCharacterInDecCharRef)
        );
    }
}

/// Represents markup of the form `&#x1F49A;` which represents a
/// character.
///
/// The [XML 1.0 specification](https://www.w3.org/TR/xml/#NT-CharRef)
/// requires that any `HexCharRef` must correspond to a valid XML 1.0
/// character.
#[derive(Debug, PartialEq)]
pub struct HexCharRef {
    character: char,
}
impl HexCharRef {
    /// Create a `HexCharRef` checking that the supplied character is
    /// a valid [XML 1.0](https://www.w3.org/TR/xml/#NT-CharRef)
    /// character.
    pub fn new(c: char) -> Result<HexCharRef, Error> {
        if is_valid_char(c) {
            Ok(HexCharRef::new_unchecked(c))
        } else {
            Err(Error::IllegalCharacterInHexCharRef)
        }
    }

    /// Create a `HexCharRef` *without* checking that the supplied character is
    /// a valid XML 1.0 character.
    pub fn new_unchecked(c: char) -> HexCharRef {
        HexCharRef { character: c }
    }

    /// Return the character as a `char` value.
    pub fn as_char(&self) -> char {
        self.character
    }

    /// Return the character as a `u32` value.
    pub fn as_u32(&self) -> u32 {
        self.character as u32
    }

    /// Parse a `HexCharRef` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<HexCharRef>, Error> {
        if !scanner.munch_hex_char_ref_start() {
            return Ok(None);
        }

        let u32_value = scanner.read_hexidecimal()?;
        scanner.munch_ref_end()?;

        if let Some(c) = std::char::from_u32(u32_value) {
            Ok(Some(HexCharRef::new(c)?))
        } else {
            Err(Error::IllegalUnicodeValue)
        }
    }

    /// Serialize the `HexCharRef` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "&#x{:X};", self.as_u32())
    }

    /// Write the `HexCharRef` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "&#x{:X};", self.as_u32())
    }
}
impl fmt::Display for HexCharRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.character)
    }
}

#[cfg(test)]
mod hex_char_ref_tests {
    use super::*;

    #[test]
    fn new() {
        let result = HexCharRef::new('A');
        assert!(result.is_ok());

        let hex_char_ref = result.unwrap();
        assert_eq!(hex_char_ref.as_char(), 'A');
        assert_eq!(hex_char_ref.as_u32(), 65);

        let result = HexCharRef::new('\0');
        assert!(result.is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("&#x1F49A;".chars());
        assert_eq!(
            HexCharRef::parse(&mut scanner),
            Ok(Some(HexCharRef::new_unchecked('💚')))
        );

        let mut scanner = Scanner::new(" &#x1F49A;".chars());
        assert_eq!(HexCharRef::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("&#x000;".chars());
        assert_eq!(
            HexCharRef::parse(&mut scanner),
            Err(Error::IllegalCharacterInHexCharRef)
        );
    }
}

use crate::Name;

/// Represents an entity reference e.g. `&amp;`.
///
/// See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-EntityRef) for
/// details.
#[derive(Debug, PartialEq)]
pub struct EntityRef {
    name: Name,
}
impl EntityRef {
    /// Create a new `EntityRef`, checking that it exists in the provided `EntityRefMap`.
    pub fn new(name: Name, entity_ref_map: &dyn EntityRefMap) -> Result<EntityRef, Error> {
        entity_ref_map.decode(&name)?;

        Ok(EntityRef { name })
    }

    /// Create a new `EntityRef`, *without* checking that it exists in
    /// the provided `EntityRefMap`.
    pub fn new_unchecked(name: Name) -> EntityRef {
        EntityRef { name }
    }

    /// Return a refernce to this enity references name.
    pub fn name(&self) -> &Name {
        &self.name
    }

    /// Decode this entity reference using the providied `EntityRefMap`.
    pub fn decode(&self, entity_ref_map: &dyn EntityRefMap) -> Result<String, Error> {
        entity_ref_map.decode(&self.name)
    }

    /// Parse an `EntityRef` using the provided `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<EntityRef>, Error> {
        if !scanner.munch_entity_ref_start() {
            return Ok(None);
        }

        let name = match Name::parse(scanner) {
            Some(name) => name,
            None => return Err(Error::MissingNameAfterEntityRefStart),
        };

        scanner.munch_ref_end()?;

        Ok(Some(EntityRef::new_unchecked(name)))
    }

    /// Serialize the `EntityRef` using the provided Writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "&{};", self.name)
    }

    /// Write the `EntityRef` using the provided Writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "&{};", self.name)
    }
}
impl fmt::Display for EntityRef {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "&{};", self.name)
    }
}

/// Implemented by any type which is used to decode entity references
/// to their strings.
pub trait EntityRefMap {
    fn decode(&self, name: &Name) -> Result<String, Error>;
}

/// An implementation of `EntityRefMap` which provideds expansions of
/// all the entity references predefined by XML.
pub struct XmlEntityRefMap {}
impl Default for XmlEntityRefMap {
    fn default() -> Self {
        XmlEntityRefMap {}
    }
}
impl EntityRefMap for XmlEntityRefMap {
    fn decode(&self, name: &Name) -> Result<String, Error> {
        match name.as_str() {
            "quot" => Ok(String::from("\"")),
            "amp" => Ok(String::from("&")),
            "apos" => Ok(String::from("'")),
            "lt" => Ok(String::from("<")),
            "gt" => Ok(String::from(">")),
            _ => Err(Error::UnknownEntityRefName),
        }
    }
}
