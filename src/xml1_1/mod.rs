/// Returns `true` if `c` is a valid XML 1.1 character. See the [XML
/// 1.1
/// specification](https://www.w3.org/TR/2006/REC-xml11-20060816/#charsets)
/// for more details.
pub fn is_valid_char(c: char) -> bool {
    matches!(c,
             '\u{1}'..='\u{D7FF}' |
             '\u{E000}'..='\u{FFFD}' |
             '\u{10000}'..='\u{10FFFF}')
}

/// Returns `true` if `c` is a restricted XML 1.1 character. See the [XML
/// 1.1
/// specification](https://www.w3.org/TR/2006/REC-xml11-20060816/#charsets)
/// for more details.
pub fn is_restricted_char(c: char) -> bool {
    matches!(c,
             '\u{1}'..='\u{8}' |
             '\u{B}'..='\u{C}' |
             '\u{E}'..='\u{1F}' |
             '\u{7F}'..='\u{84}' |
             '\u{86}'..='\u{9F}'
    )
}

/// Returns `true` if `c` is a *discouraged* XML 1.1 character. See the [XML
/// 1.1
/// specification](https://www.w3.org/TR/2006/REC-xml11-20060816/#charsets)
/// for more details.
pub fn is_discouraged_char(c: char) -> bool {
    matches!(c,
         '\u{1}'..='\u{8}' | '\u{B}'..='\u{C}' | '\u{E}'..='\u{1F}' |
         '\u{7F}'..='\u{84}' | '\u{86}'..='\u{9F}' | '\u{FDD0}'..='\u{FDDF}' |
         '\u{1FFFE}'..='\u{1FFFF}' | '\u{2FFFE}'..='\u{2FFFF}' | '\u{3FFFE}'..='\u{3FFFF}' |
         '\u{4FFFE}'..='\u{4FFFF}' | '\u{5FFFE}'..='\u{5FFFF}' | '\u{6FFFE}'..='\u{6FFFF}' |
         '\u{7FFFE}'..='\u{7FFFF}' | '\u{8FFFE}'..='\u{8FFFF}' | '\u{9FFFE}'..='\u{9FFFF}' |
         '\u{AFFFE}'..='\u{AFFFF}' | '\u{BFFFE}'..='\u{BFFFF}' | '\u{CFFFE}'..='\u{CFFFF}' |
         '\u{DFFFE}'..='\u{DFFFF}' | '\u{EFFFE}'..='\u{EFFFF}' | '\u{FFFFE}'..='\u{FFFFF}' |
         '\u{10FFFE}'..='\u{10FFFF}'
    )
}
