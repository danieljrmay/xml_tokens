use crate::scanner::Scanner;
use crate::xml1_0::attribute::Attribute;
use crate::xml1_0::namespace::Namespace;
use crate::xml1_0::reference::EntityRefMap;
use crate::Error;
use crate::QName;
use std::fmt::Write;

/// Represents an element `STag` or an `EmptyTag`. See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-element) for details.
#[derive(Debug, PartialEq)]
pub enum EmptyOrSTag {
    EmptyTag(EmptyTag),
    STag(STag),
}
impl EmptyOrSTag {
    /// Parse a `EmptyorSTag` using the provided `scanner`. If `Ok(None)` is
    /// returned then no characters have been consumed by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<EmptyOrSTag>, Error> {
        if !scanner.munch_s_tag_or_emtpy_tag_start() {
            return Ok(None);
        }

        let qname = match QName::parse(scanner)? {
            Some(qname) => qname,
            None => return Err(Error::MissingQNameAfterEmptyOrSTagStart),
        };

        let mut namespaces = Vec::new();
        let mut attributes = Vec::new();

        loop {
            if scanner.munch_empty_tag_end() {
                return Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                    qname, attributes, namespaces,
                ))));
            } else if scanner.munch_s_tag_end() {
                return Ok(Some(EmptyOrSTag::STag(STag::new(
                    qname, attributes, namespaces,
                ))));
            } else {
                scanner.require_whitespace()?;
            }

            if let Some(namespace) = Namespace::parse(scanner)? {
                namespaces.push(namespace);
            } else if let Some(attribute) = Attribute::parse(scanner)? {
                attributes.push(attribute);
            } else if scanner.munch_empty_tag_end() {
                return Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                    qname, attributes, namespaces,
                ))));
            } else if scanner.munch_s_tag_end() {
                return Ok(Some(EmptyOrSTag::STag(STag::new(
                    qname, attributes, namespaces,
                ))));
            } else {
                return Err(Error::MissingEmptyOrSTagEnd);
            }
        }
    }

    /// Serialize the `EmptyTag` or `STag` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        match self {
            EmptyOrSTag::EmptyTag(empty_tag) => empty_tag.format(w),
            EmptyOrSTag::STag(s_tag) => s_tag.format(w),
        }
    }

    /// Write the `EmptyTag` or `STag` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        match self {
            EmptyOrSTag::EmptyTag(empty_tag) => empty_tag.write(w),
            EmptyOrSTag::STag(s_tag) => s_tag.write(w),
        }
    }
}

#[cfg(test)]
mod empty_tag_or_s_tag_tests {
    use super::*;
    use crate::xml1_0::attribute::{AttValue, AttValueData};
    use crate::xml1_0::namespace::NamespaceValue;
    use crate::NcName;
    use std::convert::TryFrom;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("<legal_empty_tag/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::new_unchecked(String::from("legal_empty_tag"))),
                vec![],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new(" <tag_not_started_so_should_get_none/>".chars());
        assert_eq!(EmptyOrSTag::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<legal_empty_tag \t \r \n />".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::new_unchecked(String::from("legal_empty_tag"))),
                vec![],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new("<prefix:local/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::new_unchecked(String::from("prefix"))),
                    NcName::new_unchecked(String::from("local"))
                ),
                vec![],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new("<prefix:local \t \r \n />".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::new_unchecked(String::from("prefix"))),
                    NcName::new_unchecked(String::from("local"))
                ),
                vec![],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new("< prefix:local/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Err(Error::MissingQNameAfterEmptyOrSTagStart)
        );

        let mut scanner = Scanner::new("<prefix: local/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Err(Error::MissingLocalPartOfQName)
        );

        let mut scanner = Scanner::new("<prefix:local    ".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Err(Error::MissingEmptyOrSTagEnd)
        );

        let mut scanner = Scanner::new("<prefix:local xmlns:pfx='http://my.namespace'/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::new_unchecked(String::from("prefix"))),
                    NcName::new_unchecked(String::from("local"))
                ),
                vec![],
                vec![Namespace::new(
                    Some(NcName::try_from("pfx").unwrap()),
                    NamespaceValue::try_from("http://my.namespace").unwrap()
                )]
            ))))
        );

        let mut scanner =
            Scanner::new("<prefix:local   xmlns:pfx  =  'http://my.namespace'  />".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::new_unchecked(String::from("prefix"))),
                    NcName::new_unchecked(String::from("local"))
                ),
                vec![],
                vec![Namespace::new(
                    Some(NcName::try_from("pfx").unwrap()),
                    NamespaceValue::try_from("http://my.namespace").unwrap()
                )]
            ))))
        );

        let mut scanner = Scanner::new("<prefix:local pfx:name='my value'/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::new_unchecked(String::from("prefix"))),
                    NcName::new_unchecked(String::from("local"))
                ),
                vec![Attribute::new(
                    QName::new(
                        Some(NcName::try_from("pfx").unwrap()),
                        NcName::try_from("name").unwrap()
                    ),
                    AttValue::new(vec![AttValueData::CharData(String::from("my value"))])
                )],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new("<svg y=\"1\"/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::new_unchecked(String::from("svg"))),
                vec![Attribute::new(
                    QName::new(None, NcName::try_from("y").unwrap()),
                    AttValue::new(vec![AttValueData::CharData(String::from("1"))])
                )],
                vec![]
            ))))
        );

        let mut scanner = Scanner::new("<svg xmlns=\"http://my.ns\" y=\"1\"/>".chars());
        assert_eq!(
            EmptyOrSTag::parse(&mut scanner),
            Ok(Some(EmptyOrSTag::EmptyTag(EmptyTag::new(
                QName::new(None, NcName::new_unchecked(String::from("svg"))),
                vec![Attribute::new(
                    QName::new(None, NcName::try_from("y").unwrap()),
                    AttValue::new(vec![AttValueData::CharData(String::from("1"))])
                )],
                vec![Namespace::new(
                    None,
                    NamespaceValue::try_from("http://my.ns").unwrap()
                )]
            ))))
        );

        // Todo STag versions of these tests
    }
}

/// Represents an element `EmptyTag`. See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-element) for details.
#[derive(Debug, PartialEq)]
pub struct EmptyTag {
    qname: QName,
    attributes: Vec<Attribute>,
    namespaces: Vec<Namespace>,
}
impl EmptyTag {
    // Create an `EmptyTag`.
    pub fn new(qname: QName, attributes: Vec<Attribute>, namespaces: Vec<Namespace>) -> EmptyTag {
        EmptyTag {
            qname,
            attributes,
            namespaces,
        }
    }

    /// Return a reference to the qname for this `EmptyTag`.
    pub fn qname(&self) -> &QName {
        &self.qname
    }

    /// Return a reference to the lit of attributes for this `EmptyTag`.
    pub fn attributes(&self) -> &Vec<Attribute> {
        &self.attributes
    }

    /// Return a reference to the lit of namespaces for this `EmptyTag`.
    pub fn namespaces(&self) -> &Vec<Namespace> {
        &self.namespaces
    }

    /// Deduplicate whitespace characters in attribute
    /// values. Replaces neibouring whitespace characters with a
    /// single space character.
    pub fn deduplicate_whitespace(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.deduplicate_whitespace();
        }
    }

    /// Normalize whitespace characters in attribute
    /// values. Replaces neibouring whitespace characters with a
    /// single space character.
    pub fn normalize_whitespace(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.normalize_whitespace();
        }
    }

    /// Merge attribute `AttValueData` where possible.
    pub fn merge_attribute_value_data(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.merge();
        }
    }

    /// Replace as many refernces as possible in this elements attributes
    /// using the provided `EntityRefMap`.
    pub fn replace_references(&mut self, entity_ref_map: &dyn EntityRefMap) {
        for attribute in self.attributes.iter_mut() {
            attribute.replace_references(entity_ref_map);
        }
    }

    /// Serialize the `EmptyTag` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<{}", self.qname)?;

        for namespace in &self.namespaces {
            namespace.format(w)?;
        }

        for attribute in &self.attributes {
            attribute.format(w)?;
        }

        write!(w, "/>")
    }

    /// Write the `EmptyTag` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<{}", self.qname)?;

        for namespace in &self.namespaces {
            namespace.write(w)?;
        }

        for attribute in &self.attributes {
            attribute.write(w)?;
        }

        write!(w, "/>")
    }
}

/// Represents an element `STag`. See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-element) for details.
#[derive(Debug, PartialEq)]
pub struct STag {
    qname: QName,
    attributes: Vec<Attribute>,
    namespaces: Vec<Namespace>,
}
impl STag {
    /// Create a new `STag`.
    pub fn new(qname: QName, attributes: Vec<Attribute>, namespaces: Vec<Namespace>) -> STag {
        STag {
            qname,
            attributes,
            namespaces,
        }
    }

    /// Return a reference to the qname for this `STag`.
    pub fn qname(&self) -> &QName {
        &self.qname
    }

    /// Return a reference to the lit of attributes for this `STag`.
    pub fn attributes(&self) -> &Vec<Attribute> {
        &self.attributes
    }

    /// Return a reference to the lit of namespaces for this `STag`.
    pub fn namespaces(&self) -> &Vec<Namespace> {
        &self.namespaces
    }

    /// Deduplicate whitespace characters in attribute
    /// values. Replaces neibouring whitespace characters with a
    /// single space character.
    pub fn deduplicate_whitespace(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.deduplicate_whitespace();
        }
    }

    /// Normalize whitespace characters in attribute
    /// values. Replaces neibouring whitespace characters with a
    /// single space character.
    pub fn normalize_whitespace(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.normalize_whitespace();
        }
    }

    /// Merge attribute `AttValueData` where possible.
    pub fn merge_attribute_value_data(&mut self) {
        for attribute in self.attributes.iter_mut() {
            attribute.merge();
        }
    }

    /// Replace as many refernces as possible in this elements attributes
    /// using the provided `EntityRefMap`.
    pub fn replace_references(&mut self, entity_ref_map: &dyn EntityRefMap) {
        for attribute in self.attributes.iter_mut() {
            attribute.replace_references(entity_ref_map);
        }
    }

    /// Serialize the `EmptyTag` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<{}", self.qname)?;

        for namespace in &self.namespaces {
            namespace.format(w)?;
        }

        for attribute in &self.attributes {
            attribute.format(w)?;
        }

        write!(w, ">")
    }

    /// Write the `EmptyTag` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<{}", self.qname)?;

        for namespace in &self.namespaces {
            namespace.write(w)?;
        }

        for attribute in &self.attributes {
            attribute.write(w)?;
        }

        write!(w, ">")
    }
}

/// Represents an element `ETag`. See the [XML 1.0
/// specification](https://www.w3.org/TR/xml/#NT-element) for details.
#[derive(Debug, PartialEq)]
pub struct ETag {
    qname: QName,
}
impl ETag {
    // Create a new `ETag`.
    pub fn new(qname: QName) -> ETag {
        ETag { qname }
    }

    /// Return a reference to the qname for this `ETag`.
    pub fn qname(&self) -> &QName {
        &self.qname
    }

    /// Parse a `ETag` using the provided `scanner`. If `Ok(None)` is
    /// returned then no characters have been consumed by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<ETag>, Error> {
        if !scanner.munch_e_tag_start() {
            return Ok(None);
        }

        let qname = match QName::parse(scanner)? {
            Some(qname) => qname,
            None => return Err(Error::MissingQNameAfterETagStart),
        };

        scanner.munch_whitespace();
        scanner.munch_e_tag_end()?;

        Ok(Some(ETag::new(qname)))
    }

    /// Serialize the `ETag` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "</{}>", self.qname)
    }

    /// Write the `ETag` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "</{}>", self.qname)
    }
}

#[cfg(test)]
mod e_tag_tests {
    use super::*;
    use crate::NcName;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("</legal_etag>".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Ok(Some(ETag::new(QName::new(
                None,
                NcName::new_unchecked(String::from("legal_etag"))
            ))))
        );

        let mut scanner = Scanner::new(" </etag_not_started_so_should_get_none>".chars());
        assert_eq!(ETag::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("</legal_etag   \n \r \t  >".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Ok(Some(ETag::new(QName::new(
                None,
                NcName::new_unchecked(String::from("legal_etag"))
            ))))
        );

        let mut scanner = Scanner::new("</legal_etag>".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Ok(Some(ETag::new(QName::new(
                None,
                NcName::new_unchecked(String::from("legal_etag"))
            ))))
        );

        let mut scanner = Scanner::new("</legal_prefix:legal_local>".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Ok(Some(ETag::new(QName::new(
                Some(NcName::new_unchecked(String::from("legal_prefix"))),
                NcName::new_unchecked(String::from("legal_local"))
            ))))
        );

        let mut scanner = Scanner::new("</legal_prefix:legal_local \n \t \r >".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Ok(Some(ETag::new(QName::new(
                Some(NcName::new_unchecked(String::from("legal_prefix"))),
                NcName::new_unchecked(String::from("legal_local"))
            ))))
        );

        let mut scanner = Scanner::new("</ legal_prefix:legal_local \n \t \r >".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Err(Error::MissingQNameAfterETagStart)
        );

        let mut scanner = Scanner::new("</legal_prefix: legal_local \n \t \r >".chars());
        assert_eq!(
            ETag::parse(&mut scanner),
            Err(Error::MissingLocalPartOfQName)
        );

        let mut scanner = Scanner::new("</legal_prefix:legal_local \n \t \r ".chars());
        assert_eq!(ETag::parse(&mut scanner), Err(Error::MissingETagEnd));
    }
}
