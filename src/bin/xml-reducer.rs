use std::fs;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;
use std::{env, io::Write};
use xml_tokens::xml1_0::filters::CommentRemover;
use xml_tokens::xml1_0::filters::EmptyTokenRemover;
use xml_tokens::xml1_0::filters::ReferenceReplacer;
use xml_tokens::xml1_0::filters::ReferenceReplacerMode;
use xml_tokens::xml1_0::filters::WhitespaceReducer;
use xml_tokens::xml1_0::reference::XmlEntityRefMap;
use xml_tokens::xml1_0::serializer::Serializer;
use xml_tokens::xml1_0::tokenizer::Tokenizer;

/// A simple programme to which can be used to reduce the size of XML files.
///
/// This programme takes an XML filename as input from the command
/// line, processes it into tokens and then passes them through a
/// number of reducing filters to remove comments, replace references,
/// reduce whitespace and remove empty tokens before it serializises
/// the result out to the specified output file.
fn main() {
    println!("XML Reducer\n");

    let args: Vec<String> = env::args().collect();

    if args.len() != 4 {
        eprintln!("Error: Wrong number of command line arguments.");
        print_usage();
        std::process::exit(1);
    }

    let output_filename;
    if args[1] == "-o" {
        output_filename = &args[2];
    } else {
        eprintln!("Error: Expected -o switch as first command line argument.");
        print_usage();
        std::process::exit(2);
    }

    let input_filename = &args[3];

    println!("Input: {}", input_filename);
    println!("Output: {}", output_filename);

    let input_path = Path::new(input_filename);
    let output_path = Path::new(output_filename);

    let mut output_file = match File::create(output_path) {
        Ok(f) => BufWriter::new(f),
        Err(error) => panic!(
            "Error: Failed to write to {} due to this error: {}",
            output_filename, error
        ),
    };

    match fs::read_to_string(input_path) {
        Ok(input) => {
            let entity_ref_map = XmlEntityRefMap::default();
            let mut tokenizer = Tokenizer::new(input.chars());
            let mut comment_remover = CommentRemover::new(&mut tokenizer);
            let mut reference_replacer = ReferenceReplacer::new(
                &mut comment_remover,
                &entity_ref_map,
                ReferenceReplacerMode::ReplaceAllRefs,
            );
            let mut whitespace_reducer = WhitespaceReducer::new(&mut reference_replacer);
            let mut empty_token_remover = EmptyTokenRemover::new(&mut whitespace_reducer);
            let mut serializer = Serializer::new(&mut empty_token_remover);

            //match serializer.serialize(&mut output) {
            match serializer.write(&mut output_file) {
                Ok(()) => println!("Finished."),
                Err(error) => println!("Error: {}\nFailed to write output.", error),
            }
        }
        Err(_error) => panic!("Error reading input path into string."),
    }

    match output_file.flush() {
        Ok(()) => (),
        Err(error) => panic!("Error flushing buffered writer: {}", error),
    }
}

fn print_usage() {
    println!("Usage: xml-reducer -o OUTFILE INFILE");
}
