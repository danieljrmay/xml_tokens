//! This module provides a means of parsing XML markup to tokens,
//! processing those tokens in various ways, and serializing those
//! tokens back to XML markup.

/// Provides various utility methods for parsing parts of XML markup.
pub mod scanner;

/// Parts of this library which are specific to XML version 1.0.
pub mod xml1_0;

use crate::scanner::Scanner;
use std::char;
use std::fmt;

/// Return `true` if `c` is a whitespace character according to the
/// XML 1.0 and [XML 1.1](https://www.w3.org/TR/xml11/#NT-S) specifications.
fn is_whitespace(c: char) -> bool {
    matches!(c, '\u{9}' | '\u{A}' | '\u{D}' | '\u{20}')
}

/// Return `true` if `c` is a decimal digit e.g. `'0'..='9'`.
fn is_decimal_digit(c: char) -> bool {
    matches!(c, '0'..='9')
}

/// Return `true` if `c` is a hexidecimal digit e.g. `'0'..='9'`,
/// `'a'..='f'` or `'A'..='F'`.
fn is_hexidecimal_digit(c: char) -> bool {
    matches!(c, '0'..='9' | 'a'..='f' | 'A'..='F')
}

/// Return a space deduplicated `String` where duplicate
/// whitespace is replaced with a single space character.
fn deduplicate_whitespace(s: &str) -> String {
    let normalized_space = normalize_whitespace(s);

    if s.is_empty() {
        return normalized_space;
    } else if normalized_space.is_empty() {
        return String::from(" ");
    }

    let whitespace_head: bool;
    if is_whitespace(s.chars().next().unwrap()) {
        whitespace_head = true;
    } else {
        whitespace_head = false;
    }

    let whitespace_tail: bool;
    if is_whitespace(s.chars().last().unwrap()) {
        whitespace_tail = true;
    } else {
        whitespace_tail = false;
    }

    if whitespace_head && whitespace_tail {
        format!(" {} ", normalized_space)
    } else if whitespace_head {
        format!(" {}", normalized_space)
    } else if whitespace_tail {
        format!("{} ", normalized_space)
    } else {
        normalized_space
    }
}

/// Return a space normalized `String` where whitespace is trimmed
/// from the head and tail, and duplicate whitespace is replaced
/// with a single space character in the body.
fn normalize_whitespace(s: &str) -> String {
    s.split_whitespace().collect::<Vec<&str>>().join(" ")
}

/// Return a space normalized `String` but whitespace is only
/// deduplicated from the head, rather than trimmed entirely.
fn normalize_whitespace_deduplicate_head(s: &str) -> String {
    let normalized_space = normalize_whitespace(s);

    if s.is_empty() {
        return normalized_space;
    } else if normalized_space.is_empty() {
        return String::from(" ");
    }

    if is_whitespace(s.chars().next().unwrap()) {
        format!(" {}", normalized_space)
    } else {
        normalized_space
    }
}

/// Return a space normalized `String` but whitespace is only
/// deduplicated from the tail, rather than trimmed entirely.
fn normalize_whitespace_deduplicate_tail(s: &str) -> String {
    let normalized_space = normalize_whitespace(s);

    if s.is_empty() {
        return normalized_space;
    } else if normalized_space.is_empty() {
        return String::from(" ");
    }

    if is_whitespace(s.chars().last().unwrap()) {
        format!("{} ", normalized_space)
    } else {
        normalized_space
    }
}

/// Represents the quotation characters `'"'` (double quotes) and
/// `'\''` (single quotes).
///
/// These occur in various places in XML markup e.g. surrounding
/// attribute values, etc.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Quotes {
    Single,
    Double,
}
impl Quotes {
    pub fn parse(scanner: &mut Scanner) -> Result<Quotes, Error> {
        match scanner.next() {
            Some('"') => Ok(Quotes::Double),
            Some('\'') => Ok(Quotes::Single),
            _ => Err(Error::MissingQuotes(scanner.index())),
        }
    }
}
impl fmt::Display for Quotes {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Quotes::Single => f.write_str("'"),
            Quotes::Double => f.write_str("\""),
        }
    }
}

/// The various error states for this library.
#[derive(Debug, PartialEq)]
pub enum Error {
    MissingXmlDeclStart(usize),
    MissingEqualsCharacter(usize),
    XmlOnePointOneDetected(usize),
    MissingVersionNum(usize),
    MissingWhitespace(usize),
    MissingVersion(usize),
    MissingQuotes(usize),
    UnmatchingQuotes(usize),
    MissingStandaloneValue(usize),
    MissingXmlDeclEnd(usize),
    MissingDoctypeDeclEnd(usize),
    MissingCommentEnd(usize),
    MissingEncName,
    IllegalEncNameStartCharacter,
    IllegalEncNameZeroLength,
    IllegalEncNameCharacter,
    IllegalNameStartCharacter,
    IllegalNameZeroLength,
    IllegalNameCharacter,
    IllegalNcNameStartCharacter,
    IllegalNcNameZeroLength,
    IllegalNcNameCharacter,
    TwoHyphensInComment,
    IllegalCharacterInComment,
    IllegalPiTargetXml,
    IllegalPiTargetStartCharacter,
    IllegalPiTargetZeroLength,
    IllegalPiTargetCharacter,
    PiDataEndInPiData,
    IllegalCharacterInPiData,
    MissingWhitespaceAtStartOfNonZeroLengthPiData,
    MissingPiEnd,
    MissingElementETagEndChar,
    ZeroLengthNamespaceValue,
    NamespaceValueNotLegalUri,
    IllegalCharacterInText,
    CdataSectionEndInText,
    IllegalCharacterInCdataSection,
    CdataSectionEndInCdataSection,
    ScannedCharacterFailedTestBeforeEndSequence,
    MissingCdataSectionEnd,
    MissingEntityReferenceEnd,
    IllegalCharacterInDecCharRef,
    IllegalCharacterInHexCharRef,
    IllegalUnicodeValue,
    MissingDecCharRefEnd,
    MissingHexCharRefEnd,
    MissingEntityRefEnd,
    MissingETagEnd,
    MissingEmptyOrSTagEnd,
    MissingLocalPartOfQName,
    MissingDoctypeDeclName,
    MissingQNameAfterEmptyOrSTagStart,
    MissingQNameAfterETagStart,
    MissingNameAfterEntityRefStart,
    MissingNamespacePrefix,
    IllegalXmlVersionNumber,
    MissingPiTarget,
    MissingRootElement,
    BadState,
    MissingHexidecimalDigits,
    MissingDecimalDigits,
    IllegalPiZeroLength,
    MissingReferenceEnd,
    ParseIntFailure,
    NotImplementedYet,
    MissingTerminatingSequence,
    IllegalCharacter,
    MunchedIllegalSequence,
    IllegalCharacterInNamespaceValue,
    MissingNamespaceValueEndQuote,
    UnknownEntityRefName,
    IllegalCharacterInAttribute,
    PeekAheadValueIsGreaterThanQueueCapacity,
}
impl std::error::Error for Error {}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ERROR: {:?}", self)
    }
}

use std::convert::TryFrom;

/// Specifies the character encoding which is being used in a
/// serialized XML document.
///
/// It appears as the value of the `encoding`
/// "attribute" of the XML declaration at the begining of an XML
/// file. It is contained by the `Token::Encoding` variant.
///
/// Its defintion is the same in [XML
/// 1.0](https://www.w3.org/TR/xml/#NT-EncodingDecl) and [XML
/// 1.1](https://www.w3.org/TR/xml11/#NT-EncName).
#[derive(Debug, PartialEq)]
pub struct EncName {
    enc_name: String,
}
impl EncName {
    /// Create a `EncName` checking that `enc_name` conforms to [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-EncodingDecl) or the [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-EncName)
    /// specifications. The specifications are the same when it comes
    /// to `EncName`.
    pub fn new(enc_name: String) -> Result<EncName, Error> {
        let mut iter = enc_name.chars();

        match iter.next() {
            Some(c) if EncName::is_valid_start_char(c) => (),
            Some(_c) => return Err(Error::IllegalEncNameStartCharacter),
            None => return Err(Error::IllegalEncNameZeroLength),
        }

        if !iter.all(EncName::is_valid_char) {
            return Err(Error::IllegalEncNameCharacter);
        }

        Ok(EncName::new_unchecked(enc_name))
    }

    /// Create a `EncName` *without* checking that `enc_name` conforms
    /// to [XML 1.0](https://www.w3.org/TR/xml/#NT-EncodingDecl) or
    /// the [XML 1.1](https://www.w3.org/TR/xml11/#NT-EncName)
    /// specifications.
    pub fn new_unchecked(enc_name: String) -> EncName {
        EncName { enc_name }
    }

    /// Parse an `EncName` using the provided `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<EncName, Error> {
        if let Some(enc_name) = scanner.read_while_with_special_start(
            16,
            EncName::is_valid_start_char,
            EncName::is_valid_char,
        ) {
            Ok(EncName::new_unchecked(enc_name))
        } else {
            Err(Error::IllegalEncNameCharacter)
        }
    }

    /// Returns `true` if `c` is a legal starting character of an
    /// `EncName` according to the the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-EncodingDecl) or the [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-EncName)
    /// specifications. The specifications are the same when it comes
    /// to `EncName`.
    pub fn is_valid_start_char(c: char) -> bool {
        matches!(c, 'A'..='Z' | 'a'..='z')
    }

    /// Returns `true` if `c` is a legal subsequent character
    /// (i.e. any character apart from the first) of an `EncName`
    /// according to the the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-EncodingDecl) or the [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-EncName)
    /// specifications. The specifications are the same when it comes
    /// to `EncName`.
    pub fn is_valid_char(c: char) -> bool {
        matches!(c, 'A'..='Z' | 'a'..='z' | '0'..='9' | '.' | '_' | '-')
    }
}
impl fmt::Display for EncName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.enc_name)
    }
}
impl TryFrom<&str> for EncName {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod enc_name_tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(
            EncName::new(String::from("utf-8")),
            Ok(EncName::new_unchecked(String::from("utf-8")))
        );

        assert_eq!(
            EncName::new(String::from("ISO8859-15")),
            Ok(EncName::new_unchecked(String::from("ISO8859-15")))
        );

        assert!(EncName::new(String::from(" utf-8")).is_err());
        assert!(EncName::new(String::from("iso8859-1 ")).is_err(),);
        assert!(EncName::new(String::from("8abcdef ")).is_err());
    }

    #[test]
    fn parse() {
        assert_eq!(
            EncName::parse(&mut Scanner::new("utf-8".chars())),
            Ok(EncName::new_unchecked(String::from("utf-8")))
        );

        assert_eq!(
            EncName::parse(&mut Scanner::new("ISO8859-15".chars())),
            Ok(EncName::new_unchecked(String::from("ISO8859-15")))
        );

        assert!(EncName::parse(&mut Scanner::new(" utf-8".chars())).is_err());
        assert_eq!(
            EncName::parse(&mut Scanner::new("utf-8 ".chars())),
            Ok(EncName::new_unchecked(String::from("utf-8")))
        );
        assert_eq!(
            EncName::parse(&mut Scanner::new("ISO8859-15'  ".chars())),
            Ok(EncName::new_unchecked(String::from("ISO8859-15")))
        );
    }

    #[test]
    fn display() {
        let enc_name = EncName::try_from("ISO8859-15").unwrap();
        assert_eq!(format!("{}", enc_name), "ISO8859-15");
    }
}

/// `Name` is used in a `DoctypeDecl` and `EntityRef`.
///
/// A related struct is `QName` which is defined by the XML Namespaces
/// specification and is used by attributes and elements.
///
/// See the [XML 1.0](https://www.w3.org/TR/xml/#NT-Name) and the [XML
/// 1.1](https://www.w3.org/TR/xml11/#NT-NameStartChar) specificaitons
/// for details, the definition of a `Name` is the same in both.
#[derive(Debug, PartialEq)]
pub struct Name {
    name: String,
}
impl Name {
    /// Create a `Name` checking that `name` conforms to the the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-Name) and [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-NameStartChar)
    /// specificaitons.  The definition of a `Name` is the same in
    /// both versions.
    pub fn new(name: String) -> Result<Name, Error> {
        let mut iter = name.chars();

        match iter.next() {
            Some(c) if Name::is_valid_start_char(c) => (),
            Some(_c) => return Err(Error::IllegalNameStartCharacter),
            None => return Err(Error::IllegalNameZeroLength),
        }

        if !iter.all(Name::is_valid_char) {
            return Err(Error::IllegalNameCharacter);
        }

        Ok(Name { name })
    }

    /// Create a `Name` *without* checking that `name` conforms to the
    /// the [XML 1.0](https://www.w3.org/TR/xml/#NT-Name) or [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-NameStartChar)
    /// specificaitons.
    pub fn new_unchecked(name: String) -> Name {
        Name { name }
    }

    /// Returns `true` if `c` is a legal starting character of an
    /// `Name` according to the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-Name) or [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-NameStartChar)
    /// specifications.
    pub fn is_valid_start_char(c: char) -> bool {
        matches!(c,
                 'a'..='z' |
                 'A'..='Z' |
                 ':' | '_' |
                 '\u{C0}'..='\u{D6}' |
                 '\u{D8}'..='\u{F6}' |
                 '\u{F8}'..='\u{2FF}' |
                 '\u{370}'..='\u{37D}' |
                 '\u{37F}'..='\u{1FFF}' |
                 '\u{200C}'..='\u{200D}' |
                 '\u{2070}'..='\u{218F}' |
                 '\u{2C00}'..='\u{2FEF}' |
                 '\u{3001}'..='\u{D7FF}' |
                 '\u{F900}'..='\u{FDCF}' |
                 '\u{FDF0}'..='\u{FFFD}' |
                 '\u{10000}'..='\u{EFFFF}')
    }

    /// Returns `true` if `c` is a legal subsequent character
    /// (i.e. any character apart from the first) for a `Name`
    /// according to the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-NameChar) or [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-NameChar) specificaitons.
    pub fn is_valid_char(c: char) -> bool {
        matches!(c,
                 'a'..='z' |
                 'A'..='Z' |
                 ':' | '_' |
                 '-' | '.' |
                 '\u{B7}' |
                 '0'..='9' |
                 '\u{C0}'..='\u{D6}' |
                 '\u{D8}'..='\u{F6}' |
                 '\u{F8}'..='\u{2FF}' |
                 '\u{300}'..='\u{37D}' |
                 '\u{37F}'..='\u{1FFF}' |
                 '\u{200C}'..='\u{200D}' |
                 '\u{203F}'..='\u{2040}' |
                 '\u{2070}'..='\u{218F}' |
                 '\u{2C00}'..='\u{2FEF}' |
                 '\u{3001}'..='\u{D7FF}' |
                 '\u{F900}'..='\u{FDCF}' |
                 '\u{FDF0}'..='\u{FFFD}' |
                 '\u{10000}'..='\u{EFFFF}')
    }

    /// Get the `Name` as a `&str`.
    pub fn as_str(&self) -> &str {
        &self.name
    }

    /// Parse a `Name` using the provided `scanner`. If `None` is
    /// returned then no characters have been consumed by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Option<Name> {
        scanner
            .read_while_with_special_start(8, Name::is_valid_start_char, Name::is_valid_char)
            .map(Name::new_unchecked)
    }
}
impl fmt::Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.name)
    }
}
impl TryFrom<&str> for Name {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod name_tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(
            Name::new(String::from("legal_name-1")),
            Ok(Name::new_unchecked(String::from("legal_name-1")))
        );

        assert_eq!(
            Name::new(String::from("LEGAL-NAME-2")),
            Ok(Name::new_unchecked(String::from("LEGAL-NAME-2")))
        );

        assert!(Name::new(String::from("-illegal-name-3")).is_err());
        assert!(Name::new(String::from(" ILLEGALNAME")).is_err(),);
        assert!(Name::new(String::from("illegal_name-*")).is_err());
    }

    #[test]
    fn parse() {
        assert_eq!(
            Name::parse(&mut Scanner::new("legal_name-1".chars())),
            Some(Name::new_unchecked(String::from("legal_name-1")))
        );

        assert_eq!(
            Name::parse(&mut Scanner::new("legal:name".chars())),
            Some(Name::new_unchecked(String::from("legal:name")))
        );

        assert!(Name::parse(&mut Scanner::new(" ILLEGALNAME".chars())).is_none());
        assert_eq!(
            Name::parse(&mut Scanner::new("LEGAL-NAME   *$%/".chars())),
            Some(Name::new_unchecked(String::from("LEGAL-NAME")))
        );
    }

    #[test]
    fn display() {
        let name = Name::try_from("_my_legal_name").unwrap();
        assert_eq!(format!("{}", name), "_my_legal_name");
    }
}

/// An `NcName` is a component part of a `QName` and `Namespace`
/// prefix.
///
///  An `NcName` is a `Name` minus the ':' character. See the
///  [Namespaces in XML
/// 1.0](https://www.w3.org/TR/xml-names/#NT-NCName) or the
/// [Namespaces in XML
/// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-NCName)
/// specificaitons for details.
#[derive(Debug, PartialEq)]
pub struct NcName {
    nc_name: String,
}
impl NcName {
    /// Create a `NcName` checking that `nc_name` conforms to the the
    /// the [Namespaces in XML
    /// 1.0](https://www.w3.org/TR/xml-names/#NT-NCName) and the
    /// [Namespaces in XML
    /// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-NCName)
    /// specificaitons. The definition is the same in both
    /// specificaitons.
    pub fn new(nc_name: String) -> Result<NcName, Error> {
        let mut iter = nc_name.chars();

        match iter.next() {
            Some(c) if NcName::is_valid_start_char(c) => (),
            Some(_c) => return Err(Error::IllegalNcNameStartCharacter),
            None => return Err(Error::IllegalNcNameZeroLength),
        }

        if !iter.all(NcName::is_valid_char) {
            return Err(Error::IllegalNcNameCharacter);
        }

        Ok(NcName { nc_name })
    }

    /// Create a `NcName` *without* checking that `nc_name` conforms
    /// to the the the [Namespaces in XML
    /// 1.0](https://www.w3.org/TR/xml-names/#NT-NCName) or the
    /// [Namespaces in XML
    /// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-NCName)
    /// specificaitons.
    pub fn new_unchecked(nc_name: String) -> NcName {
        NcName { nc_name }
    }

    /// Get this `NcName` as a `&str`.
    pub fn as_str(&self) -> &str {
        &self.nc_name
    }

    /// Returns `true` if `c` is a legal starting character of an
    /// `NcName` according to the [Namespaces in XML
    /// 1.0](https://www.w3.org/TR/xml-names/#NT-NCName) or the
    /// [Namespaces in XML
    /// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-NCName). An
    /// `NcName` has the same character defintion as a `Name` with the
    /// execption of the `':'` (colon) character.  definition of an
    /// `NcName` is the same in both specificaitons.
    pub fn is_valid_start_char(c: char) -> bool {
        matches!(c,
                 'a'..='z' |
                 'A'..='Z' |
                 '_' |
                 '\u{C0}'..='\u{D6}' |
                 '\u{D8}'..='\u{F6}' |
                 '\u{F8}'..='\u{2FF}' |
                 '\u{370}'..='\u{37D}' |
                 '\u{37F}'..='\u{1FFF}' |
                 '\u{200C}'..='\u{200D}' |
                 '\u{2070}'..='\u{218F}' |
                 '\u{2C00}'..='\u{2FEF}' |
                 '\u{3001}'..='\u{D7FF}' |
                 '\u{F900}'..='\u{FDCF}' |
                 '\u{FDF0}'..='\u{FFFD}' |
                 '\u{10000}'..='\u{EFFFF}')
    }

    /// Returns `true` if `c` is a legal subsequent character
    /// (i.e. any character apart from the first) for a `NcName`
    /// according to the [Namespaces in XML
    /// 1.0](https://www.w3.org/TR/xml-names/#NT-NCName) or the
    /// [Namespaces in XML
    /// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-NCName). An
    /// `NcName` has the same character defintion as a `Name` with the
    /// execption of the `':'` (colon) character.  definition of an
    /// `NcName` is the same in both specificaitons.
    pub fn is_valid_char(c: char) -> bool {
        matches!(c,
                 'a'..='z' |
                 'A'..='Z' |
                 '_' |
                 '-' | '.' |
                 '\u{B7}' |
                 '0'..='9' |
                 '\u{C0}'..='\u{D6}' |
                 '\u{D8}'..='\u{F6}' |
                 '\u{F8}'..='\u{2FF}' |
                 '\u{300}'..='\u{37D}' |
                 '\u{37F}'..='\u{1FFF}' |
                 '\u{200C}'..='\u{200D}' |
                 '\u{203F}'..='\u{2040}' |
                 '\u{2070}'..='\u{218F}' |
                 '\u{2C00}'..='\u{2FEF}' |
                 '\u{3001}'..='\u{D7FF}' |
                 '\u{F900}'..='\u{FDCF}' |
                 '\u{FDF0}'..='\u{FFFD}' |
                 '\u{10000}'..='\u{EFFFF}')
    }

    /// Parse a `NcName` using the provided `scanner`. If `None` is
    /// returned then no characters have been consumed by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Option<NcName> {
        scanner
            .read_while_with_special_start(8, NcName::is_valid_start_char, NcName::is_valid_char)
            .map(NcName::new_unchecked)
    }
}
impl fmt::Display for NcName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.nc_name)
    }
}
impl TryFrom<&str> for NcName {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod nc_name_tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(
            NcName::new(String::from("legal_nc_name-1")),
            Ok(NcName::new_unchecked(String::from("legal_nc_name-1")))
        );

        assert_eq!(
            NcName::new(String::from("LEGAL-NC_NAME-2")),
            Ok(NcName::new_unchecked(String::from("LEGAL-NC_NAME-2")))
        );

        assert!(NcName::new(String::from("-illegal-nc_name-3")).is_err());
        assert!(NcName::new(String::from(" ILLEGALNC_NAME")).is_err(),);
        assert!(NcName::new(String::from("illegal_nc_name-*")).is_err());
    }

    #[test]
    fn parse() {
        assert_eq!(
            NcName::parse(&mut Scanner::new("legal_nc_name-1".chars())),
            Some(NcName::new_unchecked(String::from("legal_nc_name-1")))
        );

        assert_eq!(
            NcName::parse(&mut Scanner::new("prefix:localpart".chars())),
            Some(NcName::new_unchecked(String::from("prefix")))
        );

        assert!(NcName::parse(&mut Scanner::new(" ILLEGALNC_NAME".chars())).is_none());
        assert_eq!(
            NcName::parse(&mut Scanner::new("LEGAL-NC_NAME   *$%/".chars())),
            Some(NcName::new_unchecked(String::from("LEGAL-NC_NAME")))
        );
    }

    #[test]
    fn display() {
        let nc_name = NcName::try_from("_my_legal_nc_name").unwrap();
        assert_eq!(format!("{}", nc_name), "_my_legal_nc_name");
    }
}

/// Represents a processing instruction target.
///
/// An example of a processing instruction target would be the string
/// `"php"` from the processing instruction `<?php echo("hello
/// world!");?>` — its the string immediately following the processing
/// instruction start string `"<?"`.
///
/// A processing instruction target can be any `Name` with the
/// exclusion of the reserved name `"xml"` or any case variation
/// thereof. See the [XML 1.0](https://www.w3.org/TR/xml/#NT-PITarget)
/// or [XML 1.1](https://www.w3.org/TR/xml11/#NT-PITarget)
/// specificaitons for details. The definition of `PITarget` is the
/// same for in both.
#[derive(Debug, PartialEq)]
pub struct PiTarget {
    target: String,
}
impl PiTarget {
    /// Creates a `PITarget` checking that `target` is a valid `Name`
    /// with the exclusion of the reserved `"xml"` or any case
    /// variation thereof. See the [XML
    /// 1.0](https://www.w3.org/TR/xml/#NT-PITarget) or [XML
    /// 1.1](https://www.w3.org/TR/xml11/#NT-PITarget) specifications
    /// for details. The definition of `PITarget` is the same for
    /// both.
    pub fn new(target: String) -> Result<PiTarget, Error> {
        if target.to_uppercase() == "XML" {
            return Err(Error::IllegalPiTargetXml);
        }

        let mut iter = target.chars();

        match iter.next() {
            Some(c) if Name::is_valid_start_char(c) => (),
            Some(_c) => return Err(Error::IllegalPiTargetStartCharacter),
            None => return Err(Error::IllegalPiTargetZeroLength),
        }

        if !iter.all(Name::is_valid_char) {
            return Err(Error::IllegalPiTargetCharacter);
        }

        Ok(PiTarget::new_unchecked(target))
    }

    /// Create a `PITarget` *without* checking that `target` conforms
    /// to the [XML 1.0](https://www.w3.org/TR/xml/#NT-PITarget) or
    /// [XML 1.1](https://www.w3.org/TR/xml11/#NT-PITarget)
    /// specifications.
    pub fn new_unchecked(target: String) -> PiTarget {
        PiTarget { target }
    }

    /// Parse a `PiTarget` using the provided `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<PiTarget, Error> {
        let mut pi_target = match scanner.read_while_with_special_start(
            8,
            Name::is_valid_start_char,
            Name::is_valid_char,
        ) {
            Some(pi_target) => pi_target,
            None => return Err(Error::MissingPiTarget),
        };

        if pi_target.to_uppercase() == "XML" {
            return Err(Error::IllegalPiTargetXml);
        }

        pi_target.shrink_to_fit();

        Ok(PiTarget::new_unchecked(pi_target))
    }
}
impl fmt::Display for PiTarget {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.target)
    }
}
impl TryFrom<&str> for PiTarget {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod pi_target_tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(
            PiTarget::new(String::from("php")),
            Ok(PiTarget::new_unchecked(String::from("php")))
        );

        assert_eq!(
            PiTarget::new(String::from("my-custom-target")),
            Ok(PiTarget::new_unchecked(String::from("my-custom-target")))
        );

        assert!(PiTarget::new(String::from("-illegal-target")).is_err());
        assert!(PiTarget::new(String::from("xml")).is_err(),);
        assert!(PiTarget::new(String::from("Xml")).is_err());
    }

    #[test]
    fn parse() {
        assert_eq!(
            PiTarget::parse(&mut Scanner::new("php EOF".chars())),
            Ok(PiTarget::new_unchecked(String::from("php")))
        );

        assert_eq!(
            PiTarget::parse(&mut Scanner::new("valid:target".chars())),
            Ok(PiTarget::new_unchecked(String::from("valid:target")))
        );

        assert!(PiTarget::parse(&mut Scanner::new("xml".chars())).is_err());
        assert_eq!(
            PiTarget::parse(&mut Scanner::new("xml-ok EOF".chars())),
            Ok(PiTarget::new_unchecked(String::from("xml-ok")))
        );
    }

    #[test]
    fn display() {
        let target = PiTarget::try_from("my_legal_target").unwrap();
        assert_eq!(format!("{}", target), "my_legal_target");
    }
}

/// A `QName` is a name with an optional namespace prefix.
///
/// When serialized it looks like this `"prefix:local_part"` where
/// `"prefix"` is the namespace prefix. A similar `QName` without a
/// prefix would serialize to `"local_part"`. It is used by elements
/// and attributes.
///
/// See the [Namespaces in XML
/// 1.0](https://www.w3.org/TR/xml-names/#NT-QName) or [Namespaces in
/// XML
/// 1.1](https://www.w3.org/TR/2006/REC-xml-names11-20060816/#NT-QName)
/// specifications for more details.
#[derive(Debug, PartialEq)]
pub struct QName {
    prefix: Option<NcName>,
    local_part: NcName,
}
impl QName {
    /// Create a `QName`.
    pub fn new(prefix: Option<NcName>, local_part: NcName) -> QName {
        QName { prefix, local_part }
    }

    /// Get a reference to the optional prefix of this `QName`.
    pub fn prefix(&self) -> &Option<NcName> {
        &self.prefix
    }

    /// Get a reference to the local_part of this `QName`.
    pub fn local_part(&self) -> &NcName {
        &self.local_part
    }

    /// Parse a `QName` using the provided `scanner`.  If `Ok(None)`
    /// is returned then no characters have been consumed by
    /// `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<QName>, Error> {
        let opt_nc_name = NcName::parse(scanner);

        if opt_nc_name.is_none() {
            return Ok(None);
        }

        if !scanner.munch(':') {
            return Ok(Some(QName::new(None, opt_nc_name.unwrap())));
        }

        match NcName::parse(scanner) {
            Some(local_part) => Ok(Some(QName::new(opt_nc_name, local_part))),
            None => Err(Error::MissingLocalPartOfQName),
        }
    }
}
impl fmt::Display for QName {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(pfx) = &self.prefix {
            write!(f, "{}:{}", pfx, self.local_part)
        } else {
            write!(f, "{}", self.local_part)
        }
    }
}

#[cfg(test)]
mod qname_tests {
    use super::*;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("prefix:local/>".chars());
        assert_eq!(
            QName::parse(&mut scanner),
            Ok(Some(QName::new(
                Some(NcName::new_unchecked(String::from("prefix"))),
                NcName::new_unchecked(String::from("local"))
            )))
        );

        let mut scanner = Scanner::new("local_part_only/>".chars());
        assert_eq!(
            QName::parse(&mut scanner),
            Ok(Some(QName::new(
                None,
                NcName::new_unchecked(String::from("local_part_only"))
            )))
        );

        let mut scanner = Scanner::new("/>".chars());
        assert_eq!(QName::parse(&mut scanner), Ok(None));
        assert!(scanner.munch('/'));
    }
}
