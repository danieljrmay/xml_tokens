use std::io;
use std::io::Write;
use xml_tokens::xml1_0::serializer::Serializer;
use xml_tokens::xml1_0::tokenizer::Tokenizer;

/// A simple REPL (Read Execute Print Loop) programme to aid in the
/// testing and debugging of the `xml_tokens` library.
///
/// This programme takes XML markup as input processes it into tokens
/// and then serializises them out as output.
///
/// Entering `"exit"` quits the programme.
fn main() {
    println!("XML to Tokens to XML REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                let mut tokenizer = Tokenizer::new(input.chars());
                let mut szr = Serializer::new(&mut tokenizer);
                let mut s = String::new();

                match szr.format(&mut s) {
                    Ok(()) => println!("{}", &s),
                    Err(error) => println!("ERROR: {}\n\n{}", error, s),
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
