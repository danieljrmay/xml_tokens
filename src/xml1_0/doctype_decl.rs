use crate::scanner::Scanner;
use crate::Error;
use crate::Name;
use std::fmt;
use std::fmt::Write;

/// A `DoctypeDecl` represents (the optional) document type
/// declaration near the begining of an XML file e.g. `<!DOCTYPE
/// html>`.
///
/// See the [XML 1.0](https://www.w3.org/TR/xml/#NT-doctypedecl)
/// specification for details.
#[derive(Debug, PartialEq)]
pub struct DoctypeDecl {
    name: Name,
    external_id: Option<ExternalId>,
    int_subset: Option<IntSubset>,
}
impl DoctypeDecl {
    /// Create a new `DoctypeDecl`.
    pub fn new(
        name: Name,
        external_id: Option<ExternalId>,
        int_subset: Option<IntSubset>,
    ) -> DoctypeDecl {
        DoctypeDecl {
            name,
            external_id,
            int_subset,
        }
    }

    /// Return reference to this `DoctypeDecl`'s name.
    pub fn name(&self) -> &Name {
        &self.name
    }

    /// Return reference to this `DoctypeDecl`'s optional external Id.
    pub fn external_id(&self) -> &Option<ExternalId> {
        &self.external_id
    }

    /// Return reference to this `DoctypeDecl`'s optional internal subset.
    pub fn int_subset(&self) -> &Option<IntSubset> {
        &self.int_subset
    }

    /// Parse a `DoctypeDecl` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<DoctypeDecl>, Error> {
        if !scanner.munch_doctype_decl_start() {
            return Ok(None);
        }

        scanner.require_whitespace()?;

        let name = match Name::parse(scanner) {
            Some(name) => name,
            None => return Err(Error::MissingDoctypeDeclName),
        };

        if !scanner.munch_whitespace() {
            scanner.munch_doctype_decl_end()?;
            return Ok(Some(DoctypeDecl::new(name, None, None)));
        }

        let external_id = ExternalId::parse(scanner)?;

        if external_id.is_some() && !scanner.munch_whitespace() {
            scanner.munch_doctype_decl_end()?;
            return Ok(Some(DoctypeDecl::new(name, external_id, None)));
        }

        let int_subset = IntSubset::parse(scanner)?;

        if int_subset.is_some() {
            scanner.munch_whitespace();
        }

        scanner.munch_doctype_decl_end()?;

        Ok(Some(DoctypeDecl::new(name, external_id, int_subset)))
    }

    /// Serialize the `DoctypeDecl` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<!DOCTYPE {}", &self.name)?;

        if let Some(external_id) = &self.external_id {
            write!(w, " {}", external_id)?;
        }

        if let Some(int_subset) = &self.int_subset {
            write!(w, " [{}]", int_subset)?;
        }

        write!(w, ">")
    }

    /// Write the `DoctypeDecl` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<!DOCTYPE {}", &self.name)?;

        if let Some(external_id) = &self.external_id {
            write!(w, " {}", external_id)?;
        }

        if let Some(int_subset) = &self.int_subset {
            write!(w, " [{}]", int_subset)?;
        }

        write!(w, ">")
    }
}

#[derive(Debug, PartialEq)]
pub enum ExternalId {
    Public,
    System,
}
impl ExternalId {
    /// Parse an `ExternalId` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    fn parse(scanner: &mut Scanner) -> Result<Option<ExternalId>, Error> {
        if scanner.munch_public() || scanner.munch_system() {
            Err(Error::NotImplementedYet)
        } else {
            Ok(None)
        }
    }
}
impl fmt::Display for ExternalId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO
        f.write_str("ExternalId Not Implemented Yet")
    }
}

#[derive(Debug, PartialEq)]
pub struct IntSubset {}
impl IntSubset {
    /// Parse an `IntSubset` using the provided `scanner`. If
    /// `Ok(None)` is returned then no characters have been consumed
    /// by `scanner`.
    fn parse(scanner: &mut Scanner) -> Result<Option<IntSubset>, Error> {
        if !scanner.munch('[') {
            return Ok(None);
        }

        todo!()
    }
}
impl fmt::Display for IntSubset {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO
        f.write_str("IntSubset Not Implemented Yet")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("<!DOCTYPE html>".chars());
        assert_eq!(
            DoctypeDecl::parse(&mut scanner),
            Ok(Some(DoctypeDecl::new(
                Name::new_unchecked(String::from("html")),
                None,
                None
            )))
        );

        let mut scanner = Scanner::new("<!DOCTYPE    \n  \r       html      \t      >".chars());
        assert_eq!(
            DoctypeDecl::parse(&mut scanner),
            Ok(Some(DoctypeDecl::new(
                Name::new_unchecked(String::from("html")),
                None,
                None
            )))
        );

        let mut scanner = Scanner::new(" <!DOCTYPE html>".chars());
        assert_eq!(DoctypeDecl::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<!DOCTYPExhtml>".chars());
        assert!(DoctypeDecl::parse(&mut scanner).is_err());
    }
}
