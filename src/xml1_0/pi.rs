use crate::is_whitespace;
use crate::scanner::Scanner;
use crate::xml1_0::is_valid_char;
use crate::Error;
use crate::PiTarget;
use std::convert::TryFrom;
use std::fmt;
use std::fmt::Write;

/// Represents a processing instruction in an XML 1.0 document.
///
/// See the [XML 1.0 specificaiton](https://www.w3.org/TR/xml/#NT-PI)
/// for more details.
#[derive(Debug, PartialEq)]
pub struct Pi {
    target: PiTarget,
    data: PiData,
}
impl Pi {
    /// Create a new `Pi`.
    pub fn new(target: PiTarget, data: PiData) -> Pi {
        Pi { target, data }
    }

    /// Return the target for this `Pi`.
    pub fn target(&self) -> &PiTarget {
        &self.target
    }

    /// Return the data for this `Pi`.
    pub fn data(&self) -> &PiData {
        &self.data
    }

    /// Parse a `Pi` using the provided `scanner`. If `Ok(None)` is
    /// returned then no characters have been consumed by `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<Pi>, Error> {
        if !scanner.munch_pi_start() {
            return Ok(None);
        }

        let target = PiTarget::parse(scanner)?;
        let data = PiData::parse(scanner)?;

        Ok(Some(Pi::new(target, data)))
    }

    /// Serialize the `Pi` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        write!(w, "<?{}{}?>", self.target, self.data)
    }

    /// Write the `Pi` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        write!(w, "<?{}{}?>", self.target, self.data)
    }
}

#[cfg(test)]
mod test_pi {
    use super::*;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new(" <?php?>".chars());
        assert_eq!(Pi::parse(&mut scanner), Ok(None));

        let mut scanner = Scanner::new("<?php?>".chars());
        assert_eq!(
            Pi::parse(&mut scanner),
            Ok(Some(Pi::new(
                PiTarget::new_unchecked(String::from("php")),
                PiData::new_unchecked(String::new())
            )))
        );

        let mut scanner = Scanner::new("<?php ?>".chars());
        assert_eq!(
            Pi::parse(&mut scanner),
            Ok(Some(Pi::new(
                PiTarget::new_unchecked(String::from("php")),
                PiData::new_unchecked(String::from(" "))
            )))
        );

        let mut scanner = Scanner::new("<?php*?>".chars());
        assert_eq!(
            Pi::parse(&mut scanner),
            Err(Error::MissingWhitespaceAtStartOfNonZeroLengthPiData)
        );

        let mut scanner = Scanner::new("<?php echo('hello?'); ?>".chars());
        assert_eq!(
            Pi::parse(&mut scanner),
            Ok(Some(Pi::new(
                PiTarget::new_unchecked(String::from("php")),
                PiData::new_unchecked(String::from(" echo('hello?'); "))
            )))
        );
    }
}

/// Represents processing instruction data, contained by a `Pi`.
///
/// See the [XML 1.0 specification](https://www.w3.org/TR/xml/#NT-PI)
/// for details.
#[derive(Debug, PartialEq)]
pub struct PiData {
    data: String,
}
impl PiData {
    /// Create a `PiData` checking that `data` conforms to the [XML
    /// 1.0 specification](https://www.w3.org/TR/xml/#NT-PI). Note
    /// that `data` can be an empty string. If it is a non-empty
    /// string then it must start with at least one whitespace
    /// character. The terminator for a processing instruction is the
    /// "?>" character sequence, so that is not allowed in the
    /// character data. Otherwise it can consist of any legal XML 1.0
    /// character.
    pub fn new(data: String) -> Result<PiData, Error> {
        let mut iter = data.chars();

        match iter.next() {
            Some(c) if is_whitespace(c) => (),
            Some(_c) => return Err(Error::MissingWhitespaceAtStartOfNonZeroLengthPiData),
            None => return Ok(PiData::new_unchecked(data)),
        }

        if !iter.all(is_valid_char) {
            return Err(Error::IllegalCharacterInPiData);
        }

        if data.contains("?>") {
            return Err(Error::PiDataEndInPiData);
        }

        Ok(PiData::new_unchecked(data))
    }

    /// Create a `PIData` *without* checking that `data` conforms to the
    /// [XML 1.0 specification](https://www.w3.org/TR/xml/#NT-PI).
    pub fn new_unchecked(data: String) -> PiData {
        PiData { data }
    }

    /// Parse a `PiData` using the provided `scanner`. The terminating
    /// sequence `"?>"` will be consumed on success.
    pub fn parse(scanner: &mut Scanner) -> Result<PiData, Error> {
        let data = scanner.read_while_until_terminating_sequence(64, is_valid_char, &['?', '>'])?;

        if !data.is_empty() && !data.starts_with(is_whitespace) {
            return Err(Error::MissingWhitespaceAtStartOfNonZeroLengthPiData);
        }

        Ok(PiData::new_unchecked(data))
    }
}
impl fmt::Display for PiData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.data)
    }
}
impl TryFrom<&str> for PiData {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod test_pi_data {
    use super::*;

    #[test]
    fn new() {
        assert!(PiData::new(String::new()).is_ok());
        assert!(PiData::new(String::from(" OK: First character is whitespace.")).is_ok());
        assert!(PiData::new(String::from("ERROR: First character is not whitespace.")).is_err());
        assert!(PiData::new(String::from(" ERROR: Contains ?> end sequence.")).is_err());
        assert!(PiData::new(String::from(" ERROR: Contains \0 illegal character.")).is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("?>".chars());
        assert_eq!(
            PiData::parse(&mut scanner),
            Ok(PiData::new_unchecked(String::new()))
        );

        let mut scanner = Scanner::new(" OK: First character is whitespace.?>".chars());
        assert_eq!(
            PiData::parse(&mut scanner),
            Ok(PiData::new_unchecked(String::from(
                " OK: First character is whitespace."
            )))
        );

        let mut scanner = Scanner::new("ERROR: First character is not whitespace.?>".chars());
        assert_eq!(
            PiData::parse(&mut scanner),
            Err(Error::MissingWhitespaceAtStartOfNonZeroLengthPiData)
        );

        let mut scanner = Scanner::new(
            " Two end sequences, only first will be consumed.?> end sequence.?>".chars(),
        );
        assert_eq!(
            PiData::parse(&mut scanner),
            Ok(PiData::new_unchecked(String::from(
                " Two end sequences, only first will be consumed."
            )))
        );
        assert!(scanner.munch_whitespace());
        assert!(scanner.munch_sequence(&['e', 'n', 'd']));

        let mut scanner = Scanner::new(" ERROR: Contains \0 illegal character.?>".chars());
        assert_eq!(PiData::parse(&mut scanner), Err(Error::IllegalCharacter));
    }
}
