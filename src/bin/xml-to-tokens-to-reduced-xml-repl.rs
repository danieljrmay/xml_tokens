use std::io;
use std::io::Write;
use xml_tokens::xml1_0::filters::{
    CommentRemover, EmptyTokenRemover, ReferenceReplacer, ReferenceReplacerMode, WhitespaceReducer,
};
use xml_tokens::xml1_0::reference::XmlEntityRefMap;
use xml_tokens::xml1_0::serializer::Serializer;
use xml_tokens::xml1_0::tokenizer::Tokenizer;

/// A simple REPL (Read Execute Print Loop) programme to aid in the
/// testing and debugging of the `xml_tokens` library.
///
/// This programme takes XML markup as input processes it into tokens
/// passes them through a number of reducing filters to remove
/// comments, replace references, reduce whitespace and remove empty
/// tokens before it serializises them as output.
///
/// Entering `"exit"` quits the programme.
fn main() {
    println!("XML to Tokens to Reduced XML REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                let entity_ref_map = XmlEntityRefMap::default();
                let mut tokenizer = Tokenizer::new(input.chars());
                let mut comment_remover = CommentRemover::new(&mut tokenizer);
                let mut reference_replacer = ReferenceReplacer::new(
                    &mut comment_remover,
                    &entity_ref_map,
                    ReferenceReplacerMode::ReplaceAllRefs,
                );
                let mut whitespace_reducer = WhitespaceReducer::new(&mut reference_replacer);
                let mut empty_token_remover = EmptyTokenRemover::new(&mut whitespace_reducer);
                let mut serializer = Serializer::new(&mut empty_token_remover);

                let mut s = String::new();

                match serializer.format(&mut s) {
                    Ok(()) => println!("{}", &s),
                    Err(error) => println!("ERROR: {}\n\n{}", error, s),
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
