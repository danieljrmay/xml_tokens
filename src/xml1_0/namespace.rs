use crate::scanner::Scanner;
use crate::Error;
use crate::NcName;
use std::fmt::Write;

/// Represents a namespace as defined by the [Namespaces in XML 1.0
/// specificaiton](https://www.w3.org/TR/xml-names/#concepts).
#[derive(Debug, PartialEq)]
pub struct Namespace {
    prefix: Option<NcName>,
    value: NamespaceValue,
}
impl Namespace {
    /// Create a `Namespace`.
    pub fn new(prefix: Option<NcName>, value: NamespaceValue) -> Namespace {
        Namespace { prefix, value }
    }

    /// Return the optional prefix for this `Namespace`, where `None`
    /// corresponds to a default namespace.
    pub fn prefix(&self) -> &Option<NcName> {
        &self.prefix
    }

    /// Return the value for this `Namespace`.
    pub fn value(&self) -> &NamespaceValue {
        &self.value
    }

    /// Parse a `Namespace` using the provided `scanner`. If `Ok(None)`
    /// is returned then no characters have been consumed.
    pub fn parse(scanner: &mut Scanner) -> Result<Option<Namespace>, Error> {
        if !scanner.munch_namespace_start() {
            return Ok(None);
        }

        let mut prefix: Option<NcName> = None;

        if scanner.munch_colon() {
            prefix = match NcName::parse(scanner) {
                Some(prefix) => Some(prefix),
                None => return Err(Error::MissingNamespacePrefix),
            };
        }

        scanner.munch_whitespace();
        scanner.munch_equals()?;
        scanner.munch_whitespace();

        let value = NamespaceValue::parse(scanner)?;

        Ok(Some(Namespace::new(prefix, value)))
    }

    /// Serialize the `Namespace` using the provided writer.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        match &self.prefix {
            Some(prefix) => write!(w, " xmlns:{}=\"{}\"", prefix, self.value),
            None => write!(w, " xmlns=\"{}\"", self.value),
        }
    }

    /// Write the `Namespace` using the provided writer.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        match &self.prefix {
            Some(prefix) => write!(w, " xmlns:{}=\"{}\"", prefix, self.value),
            None => write!(w, " xmlns=\"{}\"", self.value),
        }
    }
}

#[cfg(test)]
mod namespace_tests {
    use super::*;

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("xmlns='http://my.namespace.com'".chars());
        assert_eq!(
            Namespace::parse(&mut scanner),
            Ok(Some(Namespace::new(
                None,
                NamespaceValue::new_unchecked(String::from("http://my.namespace.com"))
            )))
        );

        let mut scanner = Scanner::new("xmlns:prefix='http://my.namespace.com'".chars());
        assert_eq!(
            Namespace::parse(&mut scanner),
            Ok(Some(Namespace::new(
                Some(NcName::new_unchecked(String::from("prefix"))),
                NamespaceValue::new_unchecked(String::from("http://my.namespace.com"))
            )))
        );

        let mut scanner =
            Scanner::new("xmlns:prefix   =    \"http://my.namespace.com\"    ".chars());
        assert_eq!(
            Namespace::parse(&mut scanner),
            Ok(Some(Namespace::new(
                Some(NcName::new_unchecked(String::from("prefix"))),
                NamespaceValue::new_unchecked(String::from("http://my.namespace.com"))
            )))
        );
    }
}

use crate::Quotes;
use std::convert::TryFrom;
use std::fmt;
use uriparse::URI;

/// A `NamespaceValue` is contained by the `Token::NamespaceValue`
/// variant. The [Namespaces in XML 1.0
/// specificaiton](https://www.w3.org/TR/xml-names/#concepts) defines
/// a `NamespaceValue` to be a URI with the following privisos:
///
/// * The empty string, although a legal URI, is not a legal
/// `NamespaceValue`.
///
/// * The use of a relative URI, including same-document references is
/// deprecated.
///
/// * The use of %-escaping in a `NamespaceValue` is strongly
/// discouraged, because when NamespaceValues are compared they are
/// compared as strings. This comparison is case-sensitive and no
/// %-escaping is done or undone.
#[derive(Debug, PartialEq)]
pub struct NamespaceValue {
    value: String,
}
impl NamespaceValue {
    /// Create a `NamespaceValue` checking that `value` conforms the
    /// [Namespaces in XML 1.0
    /// specificaiton](https://www.w3.org/TR/xml-names/#concepts).
    pub fn new(value: String) -> Result<NamespaceValue, Error> {
        if value.is_empty() {
            return Err(Error::ZeroLengthNamespaceValue);
        }

        if URI::try_from(value.as_str()).is_err() {
            return Err(Error::NamespaceValueNotLegalUri);
        }

        // TODO I do not think that the URI library provides a means
        // of parsing a relative URL, which although deprecated is
        // still legal.

        Ok(NamespaceValue::new_unchecked(value))
    }

    /// Create a `NamespaceValue` *without* checing that `value`
    /// conforms the [Namespaces in XML 1.0
    /// specificaiton](https://www.w3.org/TR/xml-names/#concepts).
    pub fn new_unchecked(value: String) -> NamespaceValue {
        NamespaceValue { value }
    }

    /// The characters allowed in a URI according to
    /// [RFC3986](https://www.rfc-editor.org/rfc/rfc3986.txt).
    fn is_valid_char(c: char) -> bool {
        matches!(c,
                 // reserved gen-delims
                 ':' | '/' | '?' | '#' | '[' | ']' | '@' |
                // reserved sub-delims
                '!' | '$' | '&' | '\'' | '(' | ')' | '*' | '+' | ',' | ';' | '=' |
                // unreserved
                'a'..='z' | 'A'..='Z' | '0'..='9' | '-' | '.' | '_' | '~')
    }

    /// Parse a `NamespaceValue` using the provided `scanner`.
    pub fn parse(scanner: &mut Scanner) -> Result<NamespaceValue, Error> {
        let quotes: Quotes = Quotes::parse(scanner)?;

        let mut value = String::with_capacity(16);

        match quotes {
            Quotes::Double => loop {
                match scanner.next() {
                    Some('"') => break,
                    Some(c) if NamespaceValue::is_valid_char(c) => value.push(c),
                    Some(_c) => return Err(Error::IllegalCharacterInNamespaceValue),
                    None => return Err(Error::MissingNamespaceValueEndQuote),
                }
            },
            Quotes::Single => loop {
                match scanner.next() {
                    Some('\'') => break,
                    Some(c) if NamespaceValue::is_valid_char(c) => value.push(c),
                    Some(_c) => return Err(Error::IllegalCharacterInNamespaceValue),
                    None => return Err(Error::MissingNamespaceValueEndQuote),
                }
            },
        }

        if URI::try_from(value.as_str()).is_err() {
            return Err(Error::NamespaceValueNotLegalUri);
        }

        value.shrink_to_fit();

        Ok(NamespaceValue::new_unchecked(value))
    }
}
impl fmt::Display for NamespaceValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.value)
    }
}
impl TryFrom<&str> for NamespaceValue {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::new(s.to_owned())
    }
}

#[cfg(test)]
mod namespace_value_tests {
    use super::*;

    #[test]
    fn new() {
        assert!(NamespaceValue::new(String::from("http://my.namespace.com")).is_ok());
        assert!(NamespaceValue::new(String::new()).is_err());
        assert!(NamespaceValue::new(String::from(" http://my.namespace.com")).is_err());
    }

    #[test]
    fn parse() {
        let mut scanner = Scanner::new("'http://my.namespace.com'".chars());
        assert_eq!(
            NamespaceValue::parse(&mut scanner),
            Ok(NamespaceValue::new_unchecked(String::from(
                "http://my.namespace.com"
            )))
        );

        let mut scanner = Scanner::new("\"http://my.namespace.com\"".chars());
        assert_eq!(
            NamespaceValue::parse(&mut scanner),
            Ok(NamespaceValue::new_unchecked(String::from(
                "http://my.namespace.com"
            )))
        );

        let mut scanner = Scanner::new(" \"http://my.namespace.com\"".chars());
        assert_eq!(
            NamespaceValue::parse(&mut scanner),
            Err(Error::MissingQuotes(1))
        );

        let mut scanner = Scanner::new("'http://my.namespace.com\0'".chars());
        assert_eq!(
            NamespaceValue::parse(&mut scanner),
            Err(Error::IllegalCharacterInNamespaceValue)
        );

        let mut scanner = Scanner::new("'http://my.namespace.com".chars());
        assert_eq!(
            NamespaceValue::parse(&mut scanner),
            Err(Error::MissingNamespaceValueEndQuote)
        );
    }
}
