# XML Tokens Todos

* [ ] Add tests of attribute whitespace handling.
* [ ] Add a well-formed filter.
* [ ] Improve documentation.
* [ ] Add tests.
* [ ] Create a separate `CharScanner` crate?
* [ ] Add a normalizing filter.
* [ ] Handle PUBLIC and SYSTEM  DoctypeDecls.
* [ ] Handle IntSubset of DoctypeDecls.
* [ ] Remove unequired `serialize()` methods in favour of `Display`
      implementations e.g. for `Token`.
* [ ] Allow means of parsing a relative URI for use as a `NamespaceValue`.
* [ ] Add better error reporting e.g. line number, character number, etc.
* [ ] Create XML 1.1 implementation.
