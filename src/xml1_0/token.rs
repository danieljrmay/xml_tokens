use crate::scanner::Scanner;
use crate::xml1_0::doctype_decl::DoctypeDecl;
use crate::xml1_0::element::{ETag, EmptyOrSTag, EmptyTag, STag};
use crate::xml1_0::pi::Pi;
use crate::xml1_0::reference::DecCharRef;
use crate::xml1_0::reference::EntityRef;
use crate::xml1_0::reference::HexCharRef;
use crate::xml1_0::CdataSection;
use crate::xml1_0::Comment;
use crate::xml1_0::Text;
use crate::xml1_0::XmlDecl;
use crate::Error;
use std::fmt;
use std::fmt::Write;

/// A `Token` wraps an "atomic unit" of a serialized XML 1.0 document
/// or fragment.
#[derive(Debug, PartialEq)]
pub enum Token {
    XmlDecl(XmlDecl),
    DoctypeDecl(DoctypeDecl),
    Comment(Comment),
    ProcessingInstruction(Pi),
    STag(STag),
    EmptyTag(EmptyTag),
    ETag(ETag),
    Text(Text),
    CdataSection(CdataSection),
    EntityRef(EntityRef),
    DecCharRef(DecCharRef),
    HexCharRef(HexCharRef),
    Error(Error),
}
impl Token {
    /// Serialize this `Token` to the provided writer `w`.
    pub fn format(&self, w: &mut dyn Write) -> Result<(), std::fmt::Error> {
        match self {
            Token::XmlDecl(xml_decl) => xml_decl.format(w),
            Token::DoctypeDecl(doctype_decl) => doctype_decl.format(w),
            Token::Comment(comment) => comment.format(w),
            Token::ProcessingInstruction(pi) => pi.format(w),
            Token::STag(s_tag) => s_tag.format(w),
            Token::EmptyTag(empty_tag) => empty_tag.format(w),
            Token::ETag(e_tag) => e_tag.format(w),
            Token::Text(text) => text.format(w),
            Token::CdataSection(cdata) => cdata.format(w),
            Token::EntityRef(entity_ref) => entity_ref.format(w),
            Token::DecCharRef(dec_char_ref) => dec_char_ref.format(w),
            Token::HexCharRef(hex_char_ref) => hex_char_ref.format(w),
            Token::Error(error) => write!(w, "ERROR: {:?}", error),
        }
    }

    /// Write this `Token` to the provided writer `w`.
    pub fn write(&self, w: &mut dyn std::io::Write) -> Result<(), std::io::Error> {
        match self {
            Token::XmlDecl(xml_decl) => xml_decl.write(w),
            Token::DoctypeDecl(doctype_decl) => doctype_decl.write(w),
            Token::Comment(comment) => comment.write(w),
            Token::ProcessingInstruction(pi) => pi.write(w),
            Token::STag(s_tag) => s_tag.write(w),
            Token::EmptyTag(empty_tag) => empty_tag.write(w),
            Token::ETag(e_tag) => e_tag.write(w),
            Token::Text(text) => text.write(w),
            Token::CdataSection(cdata) => cdata.write(w),
            Token::EntityRef(entity_ref) => entity_ref.write(w),
            Token::DecCharRef(dec_char_ref) => dec_char_ref.write(w),
            Token::HexCharRef(hex_char_ref) => hex_char_ref.write(w),
            Token::Error(error) => write!(w, "ERROR: {:?}", error),
        }
    }

    /// Attempt to parse a `Token::XmlDecl(xml_decl)` from the
    /// provided `scanner`.
    pub fn parse_xml_decl(scanner: &mut Scanner) -> Option<Token> {
        match XmlDecl::parse(scanner) {
            Ok(Some(xml_decl)) => Some(Token::XmlDecl(xml_decl)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::DoctypeDecl(doctype_decl)` from the
    /// provided `scanner`.
    pub fn parse_doctype_decl(scanner: &mut Scanner) -> Option<Token> {
        match DoctypeDecl::parse(scanner) {
            Ok(None) => None,
            Ok(Some(doctype_decl)) => Some(Token::DoctypeDecl(doctype_decl)),
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::Comment(comment)` from the
    /// provided `scanner`.
    pub fn parse_comment(scanner: &mut Scanner) -> Option<Token> {
        match Comment::parse(scanner) {
            Ok(Some(comment)) => Some(Token::Comment(comment)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::Pi(pi)` from the provided
    /// `scanner`.
    pub fn parse_pi(scanner: &mut Scanner) -> Option<Token> {
        match Pi::parse(scanner) {
            Ok(Some(pi)) => Some(Token::ProcessingInstruction(pi)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::EmptyTag(empty_tag)` or a
    /// `Token::STag(s_tag)` from the provided `scanner`.
    pub fn parse_empty_or_s_tag(scanner: &mut Scanner) -> Option<Token> {
        match EmptyOrSTag::parse(scanner) {
            Ok(Some(EmptyOrSTag::EmptyTag(empty_tag))) => Some(Token::EmptyTag(empty_tag)),
            Ok(Some(EmptyOrSTag::STag(s_tag))) => Some(Token::STag(s_tag)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::ETag(e_tag)` from the
    /// provided `scanner`.
    pub fn parse_e_tag(scanner: &mut Scanner) -> Option<Token> {
        match ETag::parse(scanner) {
            Ok(Some(e_tag)) => Some(Token::ETag(e_tag)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::Text(text)` from the
    /// provided `scanner`.
    pub fn parse_text(scanner: &mut Scanner) -> Option<Token> {
        match Text::parse(scanner) {
            Ok(Some(text)) => Some(Token::Text(text)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::CdataSection(cdata_section)` from the
    /// provided `scanner`.
    pub fn parse_cdata_section(scanner: &mut Scanner) -> Option<Token> {
        match CdataSection::parse(scanner) {
            Ok(Some(cdata_section)) => Some(Token::CdataSection(cdata_section)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::EntityRef(entity_ref)` from the provided
    /// `scanner`.
    pub fn parse_entity_ref(scanner: &mut Scanner) -> Option<Token> {
        match EntityRef::parse(scanner) {
            Ok(Some(entity_ref)) => Some(Token::EntityRef(entity_ref)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::DecCharRef(dec_char_ref)` from the provided
    /// `scanner`.
    pub fn parse_dec_char_ref(scanner: &mut Scanner) -> Option<Token> {
        match DecCharRef::parse(scanner) {
            Ok(Some(dec_char_ref)) => Some(Token::DecCharRef(dec_char_ref)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }

    /// Attempt to parse a `Token::HexCharRef(hex_char_ref)` from the provided
    /// `scanner`.
    pub fn parse_hex_char_ref(scanner: &mut Scanner) -> Option<Token> {
        match HexCharRef::parse(scanner) {
            Ok(Some(hex_char_ref)) => Some(Token::HexCharRef(hex_char_ref)),
            Ok(None) => None,
            Err(error) => Some(Token::Error(error)),
        }
    }
}
impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.format(f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::xml1_0::pi::PiData;
    use crate::Name;
    use crate::NcName;
    use crate::PiTarget;
    use crate::QName;
    use std::convert::TryFrom;

    #[test]
    fn parse_xml_decl() {
        let mut scanner = Scanner::new("<?xml version=\"1.0\"?>".chars());
        assert_eq!(
            Token::parse_xml_decl(&mut scanner),
            Some(Token::XmlDecl(XmlDecl::new(None, None)))
        );
    }

    #[test]
    fn parse_doctype_decl() {
        let mut scanner = Scanner::new("<!DOCTYPE html>".chars());
        assert_eq!(
            Token::parse_doctype_decl(&mut scanner),
            Some(Token::DoctypeDecl(DoctypeDecl::new(
                Name::try_from("html").unwrap(),
                None,
                None
            )))
        );
    }

    #[test]
    fn parse_comment() {
        let mut scanner = Scanner::new("<!--comment-->".chars());
        assert_eq!(
            Token::parse_comment(&mut scanner),
            Some(Token::Comment(Comment::try_from("comment").unwrap()))
        );
    }

    #[test]
    fn parse_pi() {
        let mut scanner = Scanner::new("<?php This is pi data.?>".chars());
        assert_eq!(
            Token::parse_pi(&mut scanner),
            Some(Token::ProcessingInstruction(Pi::new(
                PiTarget::try_from("php").unwrap(),
                PiData::try_from(" This is pi data.").unwrap()
            )))
        );
    }

    #[test]
    fn parse_empty_or_s_tag() {
        let mut scanner = Scanner::new("<prefix:local_part/>".chars());
        assert_eq!(
            Token::parse_empty_or_s_tag(&mut scanner),
            Some(Token::EmptyTag(EmptyTag::new(
                QName::new(
                    Some(NcName::try_from("prefix").unwrap()),
                    NcName::try_from("local_part").unwrap()
                ),
                vec![],
                vec![]
            )))
        );
    }

    #[test]
    fn parse_e_tag() {
        let mut scanner = Scanner::new("</prefix:local_part   >".chars());
        assert_eq!(
            Token::parse_e_tag(&mut scanner),
            Some(Token::ETag(ETag::new(QName::new(
                Some(NcName::try_from("prefix").unwrap()),
                NcName::try_from("local_part").unwrap()
            ))))
        );
    }

    #[test]
    fn parse_text() {
        let mut scanner = Scanner::new("This is some text.".chars());
        assert_eq!(
            Token::parse_text(&mut scanner),
            Some(Token::Text(Text::try_from("This is some text.").unwrap()))
        );
    }

    #[test]
    fn parse_cdata_section() {
        let mut scanner = Scanner::new("<![CDATA[This is a cdata section.]]>".chars());
        assert_eq!(
            Token::parse_cdata_section(&mut scanner),
            Some(Token::CdataSection(
                CdataSection::try_from("This is a cdata section.").unwrap()
            ))
        );
    }

    #[test]
    fn parse_entity_ref() {
        let mut scanner = Scanner::new("&amp;".chars());
        assert_eq!(
            Token::parse_entity_ref(&mut scanner),
            Some(Token::EntityRef(EntityRef::new_unchecked(
                Name::try_from("amp").unwrap()
            )))
        );
    }

    #[test]
    fn parse_dec_char_ref() {
        let mut scanner = Scanner::new("&#65;".chars());
        assert_eq!(
            Token::parse_dec_char_ref(&mut scanner),
            Some(Token::DecCharRef(DecCharRef::new_unchecked('A')))
        );
    }

    #[test]
    fn parse_hex_char_ref() {
        let mut scanner = Scanner::new("&#x2026;".chars());
        assert_eq!(
            Token::parse_hex_char_ref(&mut scanner),
            Some(Token::HexCharRef(HexCharRef::new_unchecked('…')))
        );
    }
}
